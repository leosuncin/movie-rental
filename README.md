# Movie Rental

## Requirements

- Node.js >= 10
- Docker >= 19.03
- Docker Compose >= 1.22
- Git >= 2.19
- Make >= 3

## Installation

```bash
git clone https://gitlab.com/leosuncin/movie-rental.git
cd movie-rental
npm install
```

## Setup

```bash
make setup
```

## Build

```bash
make build
```

Docker image

```bash
make build-docker
```

## Execution

**Development**

```bash
npm run dev
```

**Production**

```bash
npm start
```

Open [localhost:1337](http://localhost:1337/)

## Run tests

```bash
make test
```

## Deployment

Automatic deploy with [Gitlab CI](.gitlab-ci.yml) to [Heroku](https://mov-rent.herokuapp.com/)
