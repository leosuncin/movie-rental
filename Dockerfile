FROM node:8.15 AS dependencies
WORKDIR /srv/app
COPY package.json package-lock.json ./
RUN npm set progress=false &&\
  npm config set depth 0 &&\
  npm config set loglevel error &&\
  npm ci --production

FROM dependencies as build
COPY . ./
RUN npm ci && npm run build

FROM gcr.io/distroless/nodejs
WORKDIR /srv/app
COPY package.json ormconfig.js ./
COPY --from=build /srv/app/dist/ ./dist/
COPY --from=dependencies /srv/app/node_modules/ ./node_modules/
ENV NODE_ENV=production PORT=1337
CMD ["dist/server.js"]
