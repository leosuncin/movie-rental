DOCKER_IMAGE ?= leosuncin/movie-rental

setup: clean
	@docker-compose up -d
	@sleep 15
	@npm run typeorm migration:run
	@docker-compose restart pgweb
	@npm run fixtures

clean:
	@rm -rf dist coverage

build: clean
	@npm run build

build-docker:
	@docker build -t ${DOCKER_IMAGE} .

test:
	@npm test -- --coverage
