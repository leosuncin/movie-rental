const dev = process.env.NODE_ENV !== 'production';

module.exports = {
  type: 'postgres',
  url: process.env.DATABASE_URL,
  entities: [dev ? 'src/**/*.entity.ts' : 'dist/**/*.entity.js'],
  migrations: [dev ? 'src/migrations/*.ts' : 'dist/migrations/*.js'],
  subscribers: [dev ? 'src/subscribers/*.ts' : 'dist/subscribers/*.js'],
  cli: {
    migrationsDir: dev ? 'src/migrations' : 'dist/migrations',
  },
  synchronize: false,
};
