/* eslint-disable jest/no-test-callback */
import { HttpStatus, INestApplication, ValidationPipe } from '@nestjs/common';
import { TestingModule, Test } from '@nestjs/testing';
import cookieParser from 'cookie-parser';
import faker from 'faker';
import passport from 'passport';
import session from 'express-session';
import supertest from 'supertest';
import fc from 'fast-check';

import AppModule from '../src/app.module';

jest.unmock('bcrypt');

function generateUser() {
  const firstName = faker.name.firstName();
  const lastName = faker.name.lastName();

  return {
    name: `${firstName} ${lastName}`,
    email: faker.internet.exampleEmail(firstName, lastName).toLowerCase(),
    password: 'Pa$$w0rd',
  };
}

function getAuthResponseCallback(done: jest.DoneCallback) {
  return (err: Error, resp: supertest.Response) => {
    if (err) {
      console.log(resp.body);
      return done(err);
    }

    expect(resp.body.password).toBeUndefined();
    expect(resp.header['set-cookie'][0]).toMatch(/token=.*;\s+HttpOnly/);

    return done();
  };
}

describe('AuthController (e2e)', () => {
  let app: INestApplication;
  let request: supertest.SuperTest<supertest.Test>;

  beforeEach(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();

    app = moduleFixture.createNestApplication();
    app.use(cookieParser(process.env.APP_SECRET));
    app.use(
      session({
        secret: process.env.APP_SECRET as string,
        resave: false,
        saveUninitialized: false,
      }),
    );
    app.use(passport.initialize());
    app.use(passport.session());
    app.useGlobalPipes(
      new ValidationPipe({ transform: true, whitelist: true }),
    );
    await app.init();

    request = supertest(app.getHttpServer());
  });

  afterEach(async () => {
    await app.close();
  });

  it('should allow to sign up a new user', (done) => {
    request
      .post('/api/auth/register')
      .send(generateUser())
      .expect(HttpStatus.ACCEPTED)
      .end(getAuthResponseCallback(done));
  });

  it('should allow to sign in an user', (done) => {
    request
      .post('/api/auth/login')
      .send({ email: 'john@doe.me', password: 'Pa$$w0rd' })
      .expect(HttpStatus.OK)
      .end(getAuthResponseCallback(done));
  });

  it('should confirm an user after register', (done) => {
    request
      .post('/api/auth/register')
      .send(generateUser())
      .expect(HttpStatus.ACCEPTED, (err, { body: user }) => {
        if (err || !user) return done(err);

        request
          .get(`/api/auth/confirm/${user.id}`)
          .expect(HttpStatus.OK)
          .end(done);
      });
  });

  it('should not die on register', () =>
    fc.assert(
      fc.asyncProperty(
        fc.record(
          {
            name: fc.string(),
            email: fc.string(),
            password: fc.string(),
          },
          { withDeletedKeys: true },
        ),
        async (data) => {
          const resp = await request.post('/api/auth/register').send(data);
          expect(resp.status).not.toBeGreaterThanOrEqual(500);
        },
      ),
      { timeout: 200 },
    ));

  it('should not die on login', () =>
    fc.assert(
      fc.asyncProperty(
        fc.record(
          {
            email: fc.string(),
            password: fc.string(),
          },
          { withDeletedKeys: true },
        ),
        async (data) => {
          const resp = await request.post('/api/auth/login').send(data);
          expect(resp.status).not.toBeGreaterThanOrEqual(500);
        },
      ),
      { timeout: 200 },
    ));
});
