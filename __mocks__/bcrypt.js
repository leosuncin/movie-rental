module.exports = {
  compare(data, encrypted) {
    return Promise.resolve(
      data
        .split('')
        .reverse()
        .join('') === encrypted,
    );
  },
  hash(data, saltOrRounds) {
    return Promise.resolve(
      data
        .split('')
        .reverse()
        .join(''),
    );
  },
};
