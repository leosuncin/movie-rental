import {
  IsDefined,
  IsString,
  IsNotEmpty,
  IsArray,
  ArrayMinSize,
  IsInt,
  IsNumber,
  Min,
  IsEnum,
  IsDate,
  Matches,
  ArrayNotEmpty,
  IsOptional,
  ArrayMaxSize,
  ValidateNested,
  MaxDate,
} from 'class-validator';
import { Transform } from 'class-transformer';

import { MovieRate } from '../movie-rate.enum';
import { Rate } from './rate.dto';

export class MovieCreate {
  @IsDefined()
  @IsString()
  @IsNotEmpty()
  private readonly title!: string;

  @IsDefined()
  @IsString()
  @IsNotEmpty()
  private readonly description!: string;

  @IsOptional()
  @IsArray()
  @ArrayMinSize(1)
  private readonly images!: string[];

  @IsDefined()
  @Transform((value) => parseInt(value, 10))
  @IsInt()
  @Min(0)
  private readonly stock!: number;

  @IsDefined()
  @Transform((value) => parseFloat(value))
  @IsNumber()
  @Min(0)
  private readonly rentalPrice!: number;

  @IsDefined()
  @Transform((value) => parseFloat(value))
  @IsNumber()
  @Min(0)
  private readonly salePrice!: number;

  @IsDefined()
  @Transform((value) => parseInt(value, 10))
  @IsInt()
  @Min(1888)
  private readonly year!: number;

  @IsDefined()
  @IsEnum(MovieRate)
  private readonly rated!: MovieRate;

  @IsDefined()
  @IsNotEmpty()
  @Transform((value) => new Date(value))
  @IsDate()
  @MaxDate(new Date())
  private readonly releaseDate!: Date;

  @IsDefined()
  @IsString()
  @Matches(/^\d+ min$/)
  private readonly runtime!: string;

  @IsDefined()
  @IsArray()
  @ArrayNotEmpty()
  @IsString({ each: true })
  @IsNotEmpty({ each: true })
  private readonly genre!: string[];

  @IsDefined()
  @IsString()
  @IsNotEmpty()
  private readonly director!: string;

  @IsDefined()
  @IsArray()
  @ArrayNotEmpty()
  @IsString({ each: true })
  @IsNotEmpty({ each: true })
  private readonly writers!: string[];

  @IsDefined()
  @IsArray()
  @ArrayNotEmpty()
  @IsString({ each: true })
  @IsNotEmpty({ each: true })
  private readonly actors!: string[];

  @IsOptional()
  @IsString()
  @IsNotEmpty()
  private readonly production?: string;

  @IsArray()
  @ArrayMinSize(3)
  @ArrayMaxSize(3)
  @ValidateNested({ each: true })
  private readonly ratings!: Rate[];
}
