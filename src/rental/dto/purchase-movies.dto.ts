import {
  ArrayMinSize,
  IsArray,
  IsDefined,
  IsInstance,
  IsInt,
  IsNumber,
  IsPositive,
  IsString,
  IsUUID,
  Min,
  ValidateNested,
} from 'class-validator';
import { Type, Transform } from 'class-transformer';

import { Movie } from '../entities/movie.entity';

export class PurchaseDetail {
  @IsDefined()
  @IsString()
  @IsUUID('4')
  private readonly movie!: Movie;

  @IsDefined()
  @Transform(value => parseInt(value, 10))
  @IsInt()
  @Min(1)
  private readonly quantity = 1;

  @IsDefined()
  @Transform(value => parseFloat(value))
  @IsNumber()
  @IsPositive()
  private readonly price!: number;
}

export class PurchaseMovies {
  @IsDefined()
  @IsArray()
  @ArrayMinSize(1)
  @ValidateNested({ each: true })
  @IsInstance(PurchaseDetail, { each: true })
  @Type(() => PurchaseDetail)
  private readonly details!: PurchaseDetail[];
}
