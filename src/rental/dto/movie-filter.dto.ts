import { Transform } from 'class-transformer';
import {
  IsOptional,
  IsString,
  IsEnum,
  IsInt,
  Min,
  IsIn,
  IsNotEmpty,
} from 'class-validator';
import { isNullOrUndefined } from 'util';

export enum SortOption {
  'title' = 'title',
  '+title' = '+title',
  '-title' = '-title',
  'likes' = 'likes',
  '+likes' = '+likes',
  '-likes' = '-likes',
}
export type AvailableOption = 'true' | 'false' | 'all';

export class MovieFilter {
  @IsOptional()
  @IsString()
  @IsEnum(SortOption)
  readonly sort?: SortOption = SortOption.title;

  @IsOptional()
  @Transform(value => (isNullOrUndefined(value) ? value : parseInt(value)))
  @IsInt()
  @Min(1)
  readonly page?: number = 1;

  @IsOptional()
  @Transform(value => (isNullOrUndefined(value) ? value : parseInt(value)))
  @IsInt()
  @Min(1)
  readonly per_page?: number = 10;

  @IsOptional()
  @IsIn(['true', 'false', 'all'])
  @IsString()
  readonly available?: AvailableOption = 'true';

  @IsOptional()
  @IsString()
  @IsNotEmpty()
  q?: string;
}
