import {
  IsDefined,
  MaxDate,
  Min,
  IsArray,
  ArrayMinSize,
  ValidateNested,
  IsInstance,
  IsString,
  IsUUID,
  IsInt,
} from 'class-validator';
import { Transform, Type } from 'class-transformer';
import { Movie } from '../entities/movie.entity';

export class ReturnDetail {
  @IsDefined()
  @IsString()
  @IsUUID('4')
  private readonly movie!: Movie;

  @IsDefined()
  @Transform(value => parseInt(value, 10))
  @IsInt()
  @Min(1)
  private readonly quantity: number = 1;
}

export class ReturnMovies {
  @IsDefined()
  @Transform(value => new Date(value))
  @MaxDate(new Date())
  readonly returnedAt!: Date;

  @IsDefined()
  @Transform(value => parseFloat(value))
  @Min(0)
  readonly totalPaid!: number;

  @IsDefined()
  @IsArray()
  @ArrayMinSize(1)
  @ValidateNested({ each: true })
  @Type(() => ReturnDetail)
  @IsInstance(ReturnDetail, { each: true })
  readonly details!: ReturnDetail[];
}
