import { IsDefined, IsBoolean } from 'class-validator';
import { Transform } from 'class-transformer';

export class MoviePatch {
  @IsDefined()
  @Transform(value =>
    ['1', 'true'].includes(value)
      ? true
      : ['0', 'false'].includes(value)
      ? false
      : value,
  )
  @IsBoolean()
  readonly availability!: boolean;
}
