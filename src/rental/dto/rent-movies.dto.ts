import {
  IsDefined,
  IsString,
  IsUUID,
  IsInt,
  Min,
  IsNumber,
  IsPositive,
  IsDate,
  MinDate,
  IsArray,
  ValidateNested,
  ArrayMinSize,
  IsInstance,
} from 'class-validator';
import { Transform, Type } from 'class-transformer';

import { Movie } from '../entities/movie.entity';

export class RentDetail {
  @IsDefined()
  @IsString()
  @IsUUID('4')
  private readonly movie!: Movie;

  @IsDefined()
  @Transform(value => parseInt(value, 10))
  @IsInt()
  @Min(1)
  private readonly quantity: number = 1;

  @IsDefined()
  @Transform(value => parseFloat(value))
  @IsNumber()
  @IsPositive()
  private readonly price!: number;
}

export class RentMovies {
  @IsDefined()
  @Transform(value => new Date(value))
  @IsDate()
  @MinDate(new Date())
  private readonly returnDate!: Date;

  @IsDefined()
  @IsArray()
  @ArrayMinSize(1)
  @ValidateNested({ each: true })
  @Type(() => RentDetail)
  @IsInstance(RentDetail, { each: true })
  private readonly details!: RentDetail[];
}
