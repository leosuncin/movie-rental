import {
  IsOptional,
  IsString,
  IsNotEmpty,
  IsArray,
  ArrayMinSize,
  IsInt,
  IsNumber,
  Min,
  IsEnum,
  IsDate,
  Matches,
  ArrayNotEmpty,
  ArrayMaxSize,
  ValidateNested,
  MaxDate,
} from 'class-validator';
import { Transform } from 'class-transformer';

import { MovieRate } from '../movie-rate.enum';
import { Rate } from './rate.dto';

export class MovieUpdate {
  @IsOptional()
  @IsString()
  @IsNotEmpty()
  private readonly title?: string;

  @IsOptional()
  @IsString()
  @IsNotEmpty()
  private readonly description?: string;

  @IsOptional()
  @IsArray()
  @ArrayMinSize(1)
  private readonly images?: string[];

  @IsOptional()
  @IsInt()
  @Min(0)
  private readonly stock?: number;

  @IsOptional()
  @IsNumber()
  @Min(0)
  private readonly rentalPrice?: number;

  @IsOptional()
  @IsNumber()
  @Min(0)
  private readonly salePrice?: number;

  @IsOptional()
  @IsInt()
  @Min(1888)
  private readonly year?: number;

  @IsOptional()
  @IsEnum(MovieRate)
  private readonly rated?: MovieRate;

  @IsOptional()
  @IsNotEmpty()
  @Transform(value => new Date(value))
  @IsDate()
  @MaxDate(new Date())
  private readonly releaseDate?: Date;

  @IsOptional()
  @IsString()
  @Matches(/^\d+ min$/)
  private readonly runtime?: string;

  @IsOptional()
  @IsArray()
  @ArrayNotEmpty()
  @IsString({ each: true })
  @IsNotEmpty({ each: true })
  private readonly genre?: string[];

  @IsOptional()
  @IsString()
  @IsNotEmpty()
  private readonly director?: string;

  @IsOptional()
  @IsArray()
  @ArrayNotEmpty()
  @IsString({ each: true })
  @IsNotEmpty({ each: true })
  private readonly writers?: string[];

  @IsOptional()
  @IsArray()
  @ArrayNotEmpty()
  @IsString({ each: true })
  @IsNotEmpty({ each: true })
  private readonly actors?: string[];

  @IsOptional()
  @IsString()
  @IsNotEmpty()
  private readonly production?: string;

  @IsOptional()
  @ArrayMinSize(3)
  @ArrayMaxSize(3)
  @ValidateNested({ each: true })
  private readonly ratings?: Rate[];
}
