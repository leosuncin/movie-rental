import { IsDefined, IsNotEmpty, IsIn, Matches } from 'class-validator';

type CriticSource =
  | 'Internet Movie Database'
  | 'Rotten Tomatoes'
  | 'Metacritic';

export class Rate {
  @IsDefined()
  @IsNotEmpty()
  @IsIn(['Internet Movie Database', 'Rotten Tomatoes', 'Metacritic'])
  private readonly source!: CriticSource;

  @IsDefined()
  @IsNotEmpty()
  @Matches(/(\d+\.?\d*\/10)|(\d%|\d{2}%|100%)|(\d|\d{2}|1\d{2}\/100)/)
  private readonly value!: string;
}
