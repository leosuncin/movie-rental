import { Controller, Post, UseGuards, Body, Req, Get } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { Request } from 'express';

import { PurchaseService } from './purchase.service';
import { PurchaseMovies } from './dto/purchase-movies.dto';
import { User } from '../auth/user.entity';

@Controller('api/purchase')
export class PurchaseController {
  constructor(private readonly purchaseService: PurchaseService) {}

  @Post()
  @UseGuards(AuthGuard('jwt'))
  create(@Body() purchases: PurchaseMovies, @Req() req: Request) {
    const user = req.user as User;
    return this.purchaseService.create(purchases, user);
  }

  @Get('/me')
  @UseGuards(AuthGuard('jwt'))
  list(@Req() req: Request) {
    return this.purchaseService.list(req.user as User);
  }
}
