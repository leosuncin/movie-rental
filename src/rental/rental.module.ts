import { Module } from '@nestjs/common';
import { MulterModule } from '@nestjs/platform-express';
import { TypeOrmModule } from '@nestjs/typeorm';

import { Movie } from './entities/movie.entity';
import { MovieController } from './movie.controller';
import { MovieService } from './movie.service';
import { MulterConfigService } from './multer-config.service';
import { Rent } from './entities/rent.entity';
import { RentDetail } from './entities/rent-detail.entity';
import { RentService } from './rent.service';
import { RentController } from './rent.controller';
import { Purchase } from './entities/purchase.entity';
import { PurchaseDetail } from './entities/purchase-detail.entity';
import { PurchaseController } from './purchase.controller';
import { PurchaseService } from './purchase.service';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      Movie,
      Rent,
      RentDetail,
      Purchase,
      PurchaseDetail,
    ]),
    MulterModule.registerAsync({
      useClass: MulterConfigService,
    }),
  ],
  controllers: [MovieController, RentController, PurchaseController],
  providers: [MovieService, RentService, PurchaseService],
})
export class RentalModule {}
