export enum MovieRate {
  GeneralAudiences = 'G',
  ParentalGuidanceSuggested = 'PG',
  ParentsStronglyCautioned = 'PG-13',
  Restricted = 'R',
  AdultsOnly = 'NC-17',
}
