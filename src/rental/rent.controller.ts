import {
  Controller,
  Post,
  UseGuards,
  Body,
  Req,
  Put,
  SetMetadata,
  Param,
  Get,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { Request } from 'express';

import { RentService } from './rent.service';
import { RentMovies } from './dto/rent-movies.dto';
import { User } from '../auth/user.entity';
import { RolesGuard } from '../auth/guards/roles.guard';
import { ReturnMovies } from './dto/return-movies.dto';

const uuidPath =
  ':id([0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12})';

@Controller('api/rent')
export class RentController {
  constructor(private readonly rentService: RentService) {}

  @Post()
  @UseGuards(AuthGuard('jwt'))
  create(@Body() rentMovies: RentMovies, @Req() req: Request) {
    const user = req.user as User;
    return this.rentService.create(rentMovies, user);
  }

  @Put(uuidPath)
  @SetMetadata('roles', ['ADMIN_ROLE'])
  @UseGuards(AuthGuard('jwt'), RolesGuard)
  update(@Param() id: string, @Body() returnMovies: ReturnMovies) {
    return this.rentService.update(id, returnMovies);
  }

  @Get('/me')
  @UseGuards(AuthGuard('jwt'))
  list(@Req() req: Request) {
    return this.rentService.list(req.user as User);
  }
}
