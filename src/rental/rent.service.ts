import {
  Injectable,
  NotFoundException,
  BadRequestException,
  UnprocessableEntityException,
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository, IsNull } from 'typeorm';

import { Rent } from './entities/rent.entity';
import { RentMovies } from './dto/rent-movies.dto';
import { User } from '../auth/user.entity';
import { ReturnMovies } from './dto/return-movies.dto';

@Injectable()
export class RentService {
  constructor(
    @InjectRepository(Rent)
    private readonly rentRepository: Repository<Rent>,
  ) {}

  create(rentMovies: RentMovies, client: User): Promise<Rent> {
    const rent = Object.assign(new Rent(), rentMovies);
    rent.client = client;

    return this.rentRepository.save(rent);
  }

  async update(id: string, returnMovies: ReturnMovies): Promise<Rent> {
    const rent = await this.rentRepository.findOne(id, {
      relations: ['details'],
    });
    const sumQuantities = (prev: number, curr: any) => prev + curr.quantity;

    if (!rent)
      throw new NotFoundException(`There isn't any movie rent with id: ${id}`);

    if (rent.total > returnMovies.totalPaid)
      throw new BadRequestException(`Paid at least $ ${rent.total}`);

    rent.returnedAt = returnMovies.returnedAt;
    rent.totalPaid = returnMovies.totalPaid;
    const pending = rent.details.reduce(sumQuantities, 0);
    const returned = returnMovies.details.reduce(sumQuantities, 0);

    if (pending > returned)
      throw new UnprocessableEntityException(
        `There are ${pending - returned} pending movies`,
      );
    delete rent.client;
    delete rent.details;

    return this.rentRepository.save(rent);
  }

  list(client: User): Promise<Rent[]> {
    return this.rentRepository.find({
      where: {
        client: client.id,
        returnedAt: IsNull(),
      },
      relations: ['details', 'details.movie'],
    });
  }
}
