import { Test, TestingModule } from '@nestjs/testing';
import { getRepositoryToken } from '@nestjs/typeorm';
import httpMocks from 'node-mocks-http';

import { PurchaseController } from './purchase.controller';
import { Purchase } from './entities/purchase.entity';
import { PurchaseService } from './purchase.service';
import { PurchaseMovies } from './dto/purchase-movies.dto';

describe('Purchase Controller', () => {
  let controller: PurchaseController;
  const MockRepository = jest.fn().mockImplementation(() => ({
    save(dto: any) {
      return Promise.resolve(Object.assign(new Purchase(), dto));
    },
    find() {
      return Promise.resolve([]);
    },
  }));

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [PurchaseController],
      providers: [
        PurchaseService,
        {
          provide: getRepositoryToken(Purchase),
          useClass: MockRepository,
        },
      ],
    }).compile();

    controller = module.get<PurchaseController>(PurchaseController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  it('should purchase movies', () => {
    const purchases = {
      details: [
        {
          movie: '3a60114b-89d4-4156-9453-09b8c5b9b42c',
          quantity: 1,
          price: 10.0,
        },
      ],
    };
    const req = httpMocks.createRequest();

    return expect(
      controller.create((purchases as unknown) as PurchaseMovies, req),
    ).resolves.toBeDefined();
  });

  it('should list my purchases', () =>
    expect(
      controller.list(
        httpMocks.createRequest({
          user: { id: 'ecdcf8a4-c594-407a-a8f9-78c8e89b1d20' },
        }),
      ),
    ).resolves.toBeDefined());
});
