import { Injectable, UnsupportedMediaTypeException } from '@nestjs/common';
import {
  MulterOptionsFactory,
  MulterModuleOptions,
} from '@nestjs/platform-express';
import path from 'path';
import multer from 'multer';
import os from 'os';

@Injectable()
export class MulterConfigService implements MulterOptionsFactory {
  private storage: multer.StorageEngine;

  constructor() {
    this.storage = multer.diskStorage({
      destination(req, file, cb) {
        cb(null, os.tmpdir());
      },
      filename(req, file, cb) {
        const extension = path.extname(file.originalname);
        cb(null, Date.now().toString(16) + extension);
      },
    });
  }
  createMulterOptions(): MulterModuleOptions {
    return {
      storage: this.storage,
      fileFilter(_, file, cb): void {
        if (!/.*\.(jpg|png|jpeg)$/i.test(file.originalname)) {
          cb(
            new UnsupportedMediaTypeException('Only image files are allowed!'),
            false,
          );
        } else {
          cb(null, true);
        }
      },
    };
  }
}
