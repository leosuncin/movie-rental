import { Test, TestingModule } from '@nestjs/testing';
import { getRepositoryToken } from '@nestjs/typeorm';
import { Request } from 'express';
import faker from 'faker';
import fc, { Arbitrary } from 'fast-check';

import { MovieController } from './movie.controller';
import { MovieService } from './movie.service';
import { Movie } from './entities/movie.entity';
import { MovieCreate } from './dto/movie-create.dto';
import { MovieUpdate } from './dto/movie-update.dto.';
import { User } from '../auth/user.entity';
import {
  MovieFilter,
  SortOption,
  AvailableOption,
} from './dto/movie-filter.dto';

const buildMovie = (overrides: Partial<Movie> = {}): MovieCreate => {
  const movie = ({
    title: faker.lorem.words(),
    description: faker.lorem.paragraphs(),
    images: [faker.internet.url()],
    stock: faker.random.number({ min: 10, max: 100 }),
    rentalPrice: parseFloat(faker.finance.amount()),
    salePrice: parseFloat(faker.finance.amount()),
    year: faker.random.number({ min: 1960, max: 2019 }),
    rated: faker.random.arrayElement(['G', 'PG', 'PG-13', 'R', 'NC-17']),
    releaseDate: new Date(faker.date.past()),
    runtime: faker.fake('{{random.number(120)}} min'),
    genre: faker.lorem.words().split(' '),
    director: faker.fake('{{name.firstName}} {{name.lastName}}'),
    writers: [faker.fake('{{name.firstName}} {{name.lastName}}')],
    actors: [faker.fake('{{name.firstName}} {{name.lastName}}')],
    production: faker.company.companyName(),
    ratings: [
      {
        source: 'Internet Movie Database',
        value: faker.fake('{{random.number(10)}}/10'),
      },
      {
        source: 'Rotten Tomatoes',
        value: faker.fake('{{random.number(100)}}%'),
      },
      {
        source: 'Metacritic',
        value: faker.fake('{{random.number(100)}}/100'),
      },
    ],
  } as unknown) as MovieCreate;

  return Object.assign(movie, overrides);
};

describe('Movie Controller', () => {
  let controller: MovieController;
  const findMock = jest.fn(() => Promise.resolve([]));
  const saveMock = jest.fn(dto =>
    Promise.resolve(Object.assign(new Movie(), dto)),
  );
  const findOneMock = jest.fn(({ slug }) =>
    Promise.resolve(
      slug
        ? buildMovie({
            slug,
            likedBy: [{ id: '00000000-0000-0000-0000-000000000000' }] as User[],
          })
        : null,
    ),
  );
  const deleteMock = jest.fn(slug =>
    Promise.resolve({ raw: [], affected: !slug ? 0 : 1 }),
  );

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [MovieController],
      providers: [
        MovieService,
        {
          provide: getRepositoryToken(Movie),
          useValue: {
            find: findMock,
            save: saveMock,
            findOne: findOneMock,
            delete: deleteMock,
          },
        },
      ],
    }).compile();

    controller = module.get<MovieController>(MovieController);
  });

  afterEach(() => {
    findMock.mockClear();
    saveMock.mockClear();
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  it('should list all the movies', () =>
    expect(controller.find()).resolves.toEqual([]));

  it('should filter movies', () =>
    fc.assert(
      fc.asyncProperty(
        fc.record<MovieFilter>({
          sort: fc.constantFrom<SortOption>(...Object.values(SortOption)),
          page: fc.integer(1, 20),
          // eslint-disable-next-line @typescript-eslint/camelcase
          per_page: fc.integer(1, 10),
          available: fc.constantFrom<AvailableOption>('true', 'false', 'all'),
          q: fc.lorem(5, true),
        }),
        fc.record({
          roles: fc.subarray(
            ['USER_ROLE', 'ADMIN_ROLE', 'ANONYMOUS_ROLE'],
            1,
            3,
          ),
        }) as Arbitrary<User>,
        async (filters: MovieFilter, user: User) => {
          expect.hasAssertions();
          const where = Object.assign(
            filters.available === 'all'
              ? {}
              : { availability: expect.any(Boolean) },
            filters.q ? { title: expect.anything() } : {},
          );

          await expect(controller.find(filters, user)).resolves.toEqual([]);

          expect(findMock).toHaveBeenCalledWith({
            order: {
              [(filters.sort || '').replace(
                /^[+-]?/,
                '',
              )]: expect.stringMatching(/ASC|DESC/),
            },
            skip: expect.any(Number),
            take: expect.any(Number),
            ...(Object.keys(where).length
              ? { where: expect.objectContaining(where) }
              : {}),
          });
        },
      ),
    ));

  it('should create a new movie', async () => {
    expect.assertions(2);
    const newMovie = buildMovie();

    await expect(controller.create(newMovie)).resolves.toBeDefined();
    expect(saveMock).toHaveBeenCalled();
  });

  it('should get one movie', async () => {
    expect.assertions(2);

    await expect(
      controller.get('the-shawshank-redemption-1994'),
    ).resolves.toBeDefined();
    expect(findOneMock).toHaveBeenCalledWith({
      slug: 'the-shawshank-redemption-1994',
    });
  });

  it('should fail to get one movie', async () => {
    expect.assertions(2);

    await expect(controller.get('')).rejects.toThrow();
    expect(findOneMock).toHaveBeenCalled();
  });

  it('should update one movie', async () => {
    expect.assertions(3);
    const updates = {
      title: faker.lorem.words(),
      description: faker.lorem.paragraphs(),
      images: [faker.internet.url()],
      stock: faker.random.number({ min: 10, max: 100 }),
      rentalPrice: parseFloat(faker.finance.amount()),
      salePrice: parseFloat(faker.finance.amount()),
    };
    const slug = updates.title.toLowerCase().replace(/\s+/g, '-');

    await expect(
      controller.update(slug, (updates as unknown) as MovieUpdate),
    ).resolves.toBeDefined();
    expect(findOneMock).toHaveBeenCalledWith({ slug });
    expect(saveMock).toHaveBeenCalled();
  });

  it('should fail to update one movie', async () => {
    expect.assertions(3);
    const updates = {
      title: faker.lorem.words(),
      description: faker.lorem.paragraphs(),
      images: [faker.internet.url()],
      stock: faker.random.number({ min: 10, max: 100 }),
      rentalPrice: parseFloat(faker.finance.amount()),
      salePrice: parseFloat(faker.finance.amount()),
    };

    await expect(
      controller.update('', (updates as unknown) as MovieUpdate),
    ).rejects.toThrow();
    expect(findOneMock).toHaveBeenCalled();
    expect(saveMock).not.toHaveBeenCalled();
  });

  it('should remove one movie', async () => {
    expect.assertions(2);

    await expect(
      controller.delete('the-shawshank-redemption-1994'),
    ).resolves.toBeDefined();
    expect(deleteMock).toHaveBeenCalledWith({
      slug: 'the-shawshank-redemption-1994',
    });
  });

  it('should toggle the availability of a movie', () =>
    expect(
      controller.availability('the-shawshank-redemption-1994', {
        availability: true,
      }),
    ).resolves.toEqual({ availability: true }));

  it('should fail to toggle the availability of a non-existent movie', () =>
    expect(
      controller.availability('', {
        availability: true,
      }),
    ).rejects.toThrow("There isn't any movie with slug: "));

  it('should like a movie', () =>
    expect(
      controller.like('the-shawshank-redemption-1994', ({
        user: { id: '00000000-0000-0000-0000-000000000001' },
      } as unknown) as Request),
    ).resolves.toBe(2));

  it('should like a movie just once', () =>
    expect(
      controller.like('the-shawshank-redemption-1994', ({
        user: { id: '00000000-0000-0000-0000-000000000000' },
      } as unknown) as Request),
    ).resolves.toBe(1));

  it('should fail to like a movie', () =>
    expect(controller.like('', { user: {} } as Request)).rejects.toThrow(
      "There isn't any movie with slug: ",
    ));
});
