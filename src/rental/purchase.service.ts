import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

import { Purchase } from './entities/purchase.entity';
import { PurchaseMovies } from './dto/purchase-movies.dto';
import { User } from '../auth/user.entity';

@Injectable()
export class PurchaseService {
  constructor(
    @InjectRepository(Purchase)
    private readonly purchaseRepository: Repository<Purchase>,
  ) {}

  create(purchases: PurchaseMovies, client: User): Promise<Purchase> {
    const purchase = Object.assign(new Purchase(), purchases);
    purchase.client = client;
    purchase.purchasedAt = new Date();

    return this.purchaseRepository.save(purchase);
  }

  list(client: User): Promise<Purchase[]> {
    return this.purchaseRepository.find({
      where: {
        client: client.id,
      },
      relations: ['details', 'details.movie'],
    });
  }
}
