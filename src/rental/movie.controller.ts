import {
  Controller,
  Get,
  Post,
  Body,
  Param,
  Put,
  Delete,
  HttpCode,
  HttpStatus,
  UseInterceptors,
  UploadedFiles,
  UseGuards,
  SetMetadata,
  Patch,
  Req,
  Query,
} from '@nestjs/common';
import { FilesInterceptor } from '@nestjs/platform-express';
import { Request } from 'express';

import { MovieService } from './movie.service';
import { MovieCreate } from './dto/movie-create.dto';
import { MovieUpdate } from './dto/movie-update.dto.';
import { RolesGuard } from '../auth/guards/roles.guard';
import { MoviePatch } from './dto/movie-patch.dto';
import { User } from '../auth/user.entity';
import { MovieFilter } from './dto/movie-filter.dto';
import { AuthUser } from '../auth/user.decorator';
import { JWTAuthGuard } from '../auth/guards/jwt-auth.guard';
import { Movie } from './entities/movie.entity';

const slugPath = ':slug([a-z0-9-]+-\\d{4}$)';

@Controller('api/movie')
export class MovieController {
  constructor(private readonly movieService: MovieService) {}

  @Get()
  find(
    @Query() where?: MovieFilter,
    @AuthUser() user?: User,
  ): Promise<Movie[]> {
    if (
      !user ||
      !Array.isArray(user.roles) ||
      !user.roles.includes('ADMIN_ROLE')
    )
      Object.assign(where || {}, { available: 'true' });
    return this.movieService.find(where);
  }

  @Post()
  @SetMetadata('roles', ['ADMIN_ROLE'])
  @UseGuards(JWTAuthGuard, RolesGuard)
  @UseInterceptors(FilesInterceptor('images'))
  create(
    @Body() newMovie: MovieCreate,
    @UploadedFiles() images?: Express.Multer.File[],
  ): Promise<Movie> {
    if (Array.isArray(images))
      Object.assign(newMovie, { images: images.map((img) => img.path) });
    return this.movieService.create(newMovie);
  }

  @Get(slugPath)
  get(@Param('slug') slug: string): Promise<Movie> {
    return this.movieService.get(slug);
  }

  @Put(slugPath)
  @SetMetadata('roles', ['ADMIN_ROLE'])
  @UseGuards(JWTAuthGuard, RolesGuard)
  update(
    @Param('slug') slug: string,
    @Body() updates: MovieUpdate,
  ): Promise<Movie> {
    return this.movieService.update(slug, updates);
  }

  @Delete(slugPath)
  @HttpCode(HttpStatus.NO_CONTENT)
  @SetMetadata('roles', ['ADMIN_ROLE'])
  @UseGuards(JWTAuthGuard, RolesGuard)
  delete(@Param('slug') slug: string) {
    return this.movieService.remove(slug);
  }

  @Patch(slugPath)
  @HttpCode(HttpStatus.OK)
  @SetMetadata('roles', ['ADMIN_ROLE'])
  @UseGuards(JWTAuthGuard, RolesGuard)
  availability(@Param('slug') slug: string, @Body() toggle: MoviePatch) {
    return this.movieService.toggleAvailability(slug, toggle.availability);
  }

  @Post(':slug([a-z0-9-]+-\\d{4})/like')
  @UseGuards(JWTAuthGuard)
  like(@Param('slug') slug: string, @Req() req: Request) {
    const user = req.user as User;
    return this.movieService.like(slug, user);
  }
}
