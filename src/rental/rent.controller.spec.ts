import { Test, TestingModule } from '@nestjs/testing';
import { getRepositoryToken } from '@nestjs/typeorm';
import httpMocks from 'node-mocks-http';

import { RentController } from './rent.controller';
import { RentService } from './rent.service';
import { Rent } from './entities/rent.entity';
import { RentMovies } from './dto/rent-movies.dto';
import { ReturnMovies } from './dto/return-movies.dto';

describe('Rent Controller', () => {
  let controller: RentController;
  const tomorrow = 24 * 3600 * 1e3;
  const MockRepository = jest.fn().mockImplementation(() => ({
    save(dto: any) {
      return Promise.resolve(Object.assign(new Rent(), dto));
    },
    findOne(id: string) {
      return Promise.resolve(
        id
          ? (({
              returnDate: new Date(Date.now() + tomorrow),
              latePenalty: 0.1,
              total: 10,
              details: [
                {
                  movie: '3a60114b-89d4-4156-9453-09b8c5b9b42c',
                  quantity: 1,
                  price: 10.0,
                },
              ],
            } as unknown) as Rent)
          : null,
      );
    },
    find() {
      return Promise.resolve([]);
    },
  }));

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [RentController],
      providers: [
        RentService,
        {
          provide: getRepositoryToken(Rent),
          useClass: MockRepository,
        },
      ],
    }).compile();

    controller = module.get<RentController>(RentController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  it('should rent movies', () => {
    const rentMovies = {
      returnDate: new Date(Date.now() + tomorrow),
      details: [
        {
          movie: '3a60114b-89d4-4156-9453-09b8c5b9b42c',
          quantity: 1,
          price: 10.0,
        },
      ],
    };
    const req = httpMocks.createRequest();

    return expect(
      controller.create((rentMovies as unknown) as RentMovies, req),
    ).resolves.toBeDefined();
  });

  it('should return the movies', () => {
    const returnMovies = {
      returnedAt: new Date(),
      totalPaid: 10.0,
      details: [
        {
          movie: '3a60114b-89d4-4156-9453-09b8c5b9b42c',
          quantity: 1,
        },
      ],
    };

    return expect(
      controller.update(
        '86af4b9b-a36f-4816-82d7-97bec342e125',
        (returnMovies as unknown) as ReturnMovies,
      ),
    ).resolves.toBeDefined();
  });

  it('should fail to return the movies due not exist', () => {
    const returnMovies = {
      returnedAt: new Date(),
      totalPaid: 10.0,
      details: [
        {
          movie: '3a60114b-89d4-4156-9453-09b8c5b9b42c',
          quantity: 1,
        },
      ],
    };

    return expect(
      controller.update('', (returnMovies as unknown) as ReturnMovies),
    ).rejects.toThrow();
  });

  it('should fail to return the movies due incomplete payment', () => {
    const returnMovies = {
      returnedAt: new Date(),
      totalPaid: 9.0,
      details: [
        {
          movie: '3a60114b-89d4-4156-9453-09b8c5b9b42c',
          quantity: 1,
        },
      ],
    };

    return expect(
      controller.update(
        '86af4b9b-a36f-4816-82d7-97bec342e125',
        (returnMovies as unknown) as ReturnMovies,
      ),
    ).rejects.toThrow();
  });

  it('should fail to return the movies due pending movies', () => {
    const returnMovies = {
      returnedAt: new Date(),
      totalPaid: 10.0,
      details: [
        {
          movie: '3a60114b-89d4-4156-9453-09b8c5b9b42c',
          quantity: 0,
        },
      ],
    };

    return expect(
      controller.update(
        '86af4b9b-a36f-4816-82d7-97bec342e125',
        (returnMovies as unknown) as ReturnMovies,
      ),
    ).rejects.toThrow();
  });

  it('should list my pending rentals', () => {
    const req = httpMocks.createRequest({
      user: { id: 'ecdcf8a4-c594-407a-a8f9-78c8e89b1d20' },
    });

    return expect(controller.list(req)).resolves.toBeDefined();
  });
});
