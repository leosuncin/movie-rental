import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import lorca from 'lorca-nlp';
import snowball from 'node-snowball';
import sw from 'stopword';
import { Repository, FindManyOptions, Raw } from 'typeorm';
import { isNullOrUndefined } from 'util';

import { Movie } from './entities/movie.entity';
import { MovieCreate } from './dto/movie-create.dto';
import { MovieUpdate } from './dto/movie-update.dto.';
import { User } from '../auth/user.entity';
import { MovieFilter, SortOption } from './dto/movie-filter.dto';

@Injectable()
export class MovieService {
  constructor(
    @InjectRepository(Movie)
    private readonly movieRepository: Repository<Movie>,
  ) {}

  find(where: MovieFilter = {}): Promise<Movie[]> {
    const conditions: FindManyOptions<Movie> = {
      take: where.per_page,
      skip: (where.page! - 1) * where.per_page!,
    };

    switch (where.sort) {
      case SortOption.title:
      case SortOption['+title']:
        conditions['order'] = { title: 'ASC' };
        break;
      case SortOption['-title']:
        conditions['order'] = { title: 'DESC' };
        break;
      case SortOption.likes:
      case SortOption['+likes']:
        conditions['order'] = { likes: 'ASC' };
        break;
      case SortOption['-likes']:
        conditions['order'] = { likes: 'DESC' };
        break;
    }

    switch (where.available) {
      case 'true':
        conditions.where = conditions.where || {};
        Object.assign(conditions.where, { availability: true });
        break;
      case 'false':
        conditions.where = conditions.where || {};
        Object.assign(conditions.where, { availability: false });
        break;
    }

    if (!isNullOrUndefined(where.q) && where.q.length > 1) {
      const stems = lorca(
        snowball
          .stemword(
            sw.removeStopwords(
              lorca(where.q)
                .words()
                .get(),
            ),
          )
          .join(' '),
      )
        .uniqueWords()
        .get()
        .join('|');
      conditions.where = conditions.where || {};
      Object.assign(conditions.where, {
        title: Raw(alias => `${alias} ~* '${stems || where.q}'`),
      });
    }

    return this.movieRepository.find(conditions);
  }

  async get(slug: string): Promise<Movie> {
    const movie = await this.movieRepository.findOne({ slug });

    if (!movie)
      throw new NotFoundException(`There isn't any movie with slug: ${slug}`);

    return movie;
  }

  create(newMovie: MovieCreate): Promise<Movie> {
    const movie = Object.assign(new Movie(), newMovie);

    return this.movieRepository.save(movie);
  }

  async update(slug: string, movieUpdates: MovieUpdate): Promise<Movie> {
    const movie = await this.movieRepository.findOne({ slug });

    if (!movie)
      throw new NotFoundException(`There isn't any movie with slug: ${slug}`);

    Object.assign(movie, movieUpdates);

    return this.movieRepository.save(movie);
  }

  remove(slug: string) {
    return this.movieRepository.delete({ slug });
  }

  async toggleAvailability(
    slug: string,
    toggle: boolean,
  ): Promise<Partial<Movie>> {
    const movie = await this.movieRepository.findOne({ slug });

    if (!movie)
      throw new NotFoundException(`There isn't any movie with slug: ${slug}`);

    movie.availability = toggle;

    await this.movieRepository.save(movie);

    return { availability: movie.availability };
  }

  async like(slug: string, user: User) {
    const movie = await this.movieRepository.findOne(
      { slug },
      { relations: ['likedBy'] },
    );

    if (!movie)
      throw new NotFoundException(`There isn't any movie with slug: ${slug}`);

    if (!movie.likedBy.find(liked => liked.id === user.id)) {
      movie.likedBy = [...movie.likedBy, user];

      await this.movieRepository.save(movie);
    }

    return movie.likedBy.length;
  }
}
