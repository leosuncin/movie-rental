/* eslint-disable jest/no-try-expect */
import { transformAndValidateSync } from 'class-transformer-validator';
import { ValidationError } from 'class-validator';

import { Rent } from './rent.entity';
import { RentDetail } from './rent-detail.entity';
import { RentMovies } from '../dto/rent-movies.dto';
import { ReturnMovies } from '../dto/return-movies.dto';

describe('Rent', () => {
  it('should be defined', () => {
    expect(new Rent()).toBeDefined();
    expect(new RentDetail()).toBeDefined();
  });
});

describe('Rent DTO', () => {
  const tomorrow = 24 * 3600 * 1e3;

  it('should be defined', () => {
    expect(new RentMovies()).toBeDefined();
  });

  it('should validate to rent a movie', () => {
    const data = {
      returnDate: Date.now() + tomorrow,
      details: [
        {
          movie: '3a60114b-89d4-4156-9453-09b8c5b9b42c',
          quantity: 1,
          price: 10.0,
        },
      ],
    };

    expect(transformAndValidateSync(RentMovies, data)).toBeDefined();
  });

  it('should require return date', () => {
    const data = {
      details: [
        {
          movie: '3a60114b-89d4-4156-9453-09b8c5b9b42c',
          quantity: 1,
          price: 10.0,
        },
      ],
    };

    try {
      transformAndValidateSync(RentMovies, data);
    } catch (error) {
      expect(error).toHaveLength(1);
      expect(error[0]).toBeInstanceOf(ValidationError);
      expect(error[0]).toHaveProperty('property', 'returnDate');
      expect(error[0].constraints).toMatchObject({
        isDate: 'returnDate must be a Date instance',
        isDefined: 'returnDate should not be null or undefined',
        minDate: expect.stringMatching(
          /^minimal allowed date for returnDate is/,
        ),
      });
    }
  });

  describe('validate details', () => {
    const returnDate = Date.now() + tomorrow;

    it('not undefined', () => {
      try {
        transformAndValidateSync(RentMovies, {
          returnDate,
        });
      } catch (error) {
        expect(error).toHaveLength(1);
        expect(error[0]).toBeInstanceOf(ValidationError);
        expect(error[0]).toHaveProperty('property', 'details');
        expect(error[0].constraints).toMatchObject({
          isDefined: 'details should not be null or undefined',
        });
      }
    });

    it('is array', () => {
      try {
        transformAndValidateSync(RentMovies, {
          returnDate,
          details: '[]',
        });
      } catch (error) {
        expect(error).toHaveLength(1);
        expect(error[0]).toBeInstanceOf(ValidationError);
        expect(error[0]).toHaveProperty('property', 'details');
        expect(error[0].constraints).toMatchObject({
          arrayMinSize: 'details must contain at least 1 elements',
          isArray: 'details must be an array',
        });
      }
    });

    it('at least one', () => {
      try {
        transformAndValidateSync(RentMovies, {
          returnDate,
          details: [],
        });
      } catch (error) {
        expect(error).toHaveLength(1);
        expect(error[0]).toBeInstanceOf(ValidationError);
        expect(error[0]).toHaveProperty('property', 'details');
        expect(error[0].constraints).toMatchObject({
          arrayMinSize: 'details must contain at least 1 elements',
        });
      }
    });

    it('are objects', () => {
      try {
        transformAndValidateSync(RentMovies, {
          returnDate,
          details: [''],
        });
      } catch (error) {
        expect(error).toHaveLength(1);
        expect(error[0]).toBeInstanceOf(ValidationError);
        expect(error[0]).toHaveProperty('property', 'details');
        expect(error[0].constraints).toMatchObject({
          isInstance: 'each value in details must be an instance of RentDetail',
        });
      }
    });
  });
});

describe('Return DTO', () => {
  it('should be defined', () => expect(new ReturnMovies()).toBeDefined());

  it('should validate to return a movie', () => {
    const returnedAt = new Date();
    returnedAt.setHours(0);
    returnedAt.setMinutes(0);
    returnedAt.setSeconds(0);
    returnedAt.setMilliseconds(0);

    const data = {
      returnedAt,
      totalPaid: '10.0',
      details: [
        {
          movie: '3a60114b-89d4-4156-9453-09b8c5b9b42c',
          quantity: '1',
        },
      ],
    };

    expect(transformAndValidateSync(ReturnMovies, data)).toBeDefined();
  });

  it('should require the returned at', () => {
    const data = {
      totalPaid: '10.0',
      details: [
        {
          movie: '3a60114b-89d4-4156-9453-09b8c5b9b42c',
          quantity: '1',
        },
      ],
    };

    try {
      transformAndValidateSync(ReturnMovies, data);
    } catch (error) {
      expect(error).toHaveLength(1);
      expect(error[0]).toBeInstanceOf(ValidationError);
      expect(error[0]).toHaveProperty('property', 'returnedAt');
      expect(error[0].constraints).toMatchObject({
        isDefined: 'returnedAt should not be null or undefined',
        maxDate: expect.stringMatching(
          /^maximal allowed date for returnedAt is/,
        ),
      });
    }
  });
});
