import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  ManyToOne,
  OneToMany,
  BeforeInsert,
} from 'typeorm';

import { User } from '../../auth/user.entity';
import { RentDetail } from './rent-detail.entity';

@Entity()
export class Rent {
  @PrimaryGeneratedColumn('uuid')
  id!: string;

  @Column('decimal')
  total!: number;

  @Column()
  returnDate!: Date;

  @Column('decimal', { default: 0.1 })
  latePenalty = 0.1;

  @Column({ nullable: true })
  returnedAt?: Date;

  @Column('decimal', { nullable: true })
  totalPaid?: number;

  @ManyToOne(type => User, { nullable: false })
  client!: User;

  @OneToMany(type => RentDetail, detail => detail.rent, { cascade: true })
  details!: RentDetail[];

  @BeforeInsert()
  private beforeSave() {
    this.total = this.details.reduce(
      (prev: number, curr: RentDetail) => prev + curr.price * curr.quantity,
      0,
    );
  }
}
