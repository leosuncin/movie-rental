import { transformAndValidate } from 'class-transformer-validator';
import fc from 'fast-check';
import faker from 'faker';

import { Movie } from './movie.entity';
import { MovieCreate } from '../dto/movie-create.dto';
import { MovieUpdate } from '../dto/movie-update.dto.';
import { MoviePatch } from '../dto/movie-patch.dto';

const fakerToArbitrary = <F>(cb: (seed: number) => F): fc.Arbitrary<F> =>
  fc.integer().noBias().noShrink().map<F>(cb);

interface ExtendedMatchers extends jest.Matchers<any> {
  toBeBoolean: () => any;
}

describe('Movie Entity', () => {
  it('should be defined', () => {
    expect(new Movie()).toBeDefined();
  });
});

describe('Movie Create DTO', () => {
  const createArbitrary = (): fc.Arbitrary<MovieCreate> =>
    fakerToArbitrary<MovieCreate>((seed: number) => {
      faker.seed(seed);

      const title = faker.lorem.words();
      const description = faker.lorem.paragraphs();
      const images = [faker.internet.url()];
      const stock = faker.random.number({ min: 10, max: 100 });
      const rentalPrice = parseFloat(faker.finance.amount());
      const salePrice = parseFloat(faker.finance.amount());
      const year = faker.random.number({ min: 1960, max: 2019 });
      const rated = faker.random.arrayElement([
        'G',
        'PG',
        'PG-13',
        'R',
        'NC-17',
      ]);
      const releaseDate = new Date(faker.date.past());
      const runtime = faker.fake('{{random.number(120)}} min');
      const genre = faker.lorem.words().split(' ');
      const director = faker.fake('{{name.firstName}} {{name.lastName}}');
      const writers = Array(faker.random.number({ min: 1, max: 10 }))
        .fill('')
        .map(() => faker.fake('{{name.firstName}} {{name.lastName}}'));
      const actors = Array(faker.random.number({ min: 1, max: 10 }))
        .fill('')
        .map(() => faker.fake('{{name.firstName}} {{name.lastName}}'));
      const production = faker.company.companyName();
      const ratings = [
        {
          source: 'Internet Movie Database',
          value: faker.fake('{{random.number(10)}}/10'),
        },
        {
          source: 'Rotten Tomatoes',
          value: faker.fake('{{random.number(100)}}%'),
        },
        {
          source: 'Metacritic',
          value: faker.fake('{{random.number(100)}}/100'),
        },
      ];

      return ({
        title,
        description,
        images,
        stock,
        rentalPrice,
        salePrice,
        year,
        rated,
        releaseDate,
        runtime,
        genre,
        director,
        writers,
        actors,
        production,
        ratings,
      } as unknown) as MovieCreate;
    });

  it('should be defined', () => {
    expect(new MovieCreate()).toBeDefined();
  });

  it('should validate the create', () =>
    fc.assert(
      fc.asyncProperty<MovieCreate>(createArbitrary(), async (data) => {
        expect.hasAssertions();
        const actual = await transformAndValidate(MovieCreate, data);

        expect(actual).toBeDefined();
      }),
    ));
});

describe('Movie Update DTO', () => {
  it('should be defined', () => {
    expect(new MovieUpdate()).toBeDefined();
  });

  it('should validate the update', () =>
    fc.assert(
      fc.asyncProperty(
        fc.record<MovieUpdate>({
          title: fc.fullUnicodeString(1, 1000),
          description: fc.lorem(100),
          images: fc.array(fc.webUrl(), 1, 10),
          stock: fc.integer(1, 100),
          rentalPrice: fc.float(1, Number.MAX_VALUE),
          salePrice: fc.float(1, Number.MAX_VALUE),
          year: fc.integer(1960, 2019),
          releaseDate: fc.nat(Date.now() - 1),
          genre: fc.array(fc.string(5, 12), 1, 5),
          director: fc.lorem(3, true),
          writers: fc.array(fc.lorem(), 1, 5),
          actors: fc.array(fc.lorem(), 1, 10),
          production: fc.asciiString(5, 10),
        }),
        async (data) => {
          expect.hasAssertions();
          const actual = await transformAndValidate(MovieUpdate, data);

          expect(actual).toBeDefined();
        },
      ),
    ));
});

describe('Movie Patch DTO', () => {
  expect.extend({
    toBeBoolean(received?: any) {
      if (received == null)
        return {
          message(): string {
            return `expected ${received} to be boolean`;
          },
          pass: false,
        };
      if (typeof received !== 'boolean')
        return {
          message(): string {
            return `expected ${received} to be boolean`;
          },
          pass: false,
        };
      else
        return {
          message(): string {
            return `expected ${received} to be boolean`;
          },
          pass: true,
        };
    },
  });
  it('should be defined', () => {
    expect(new MoviePatch()).toBeDefined();
  });

  it('should validate the patch', () =>
    fc.assert(
      fc.asyncProperty(
        fc.record({
          availability: fc.constantFrom('1', 'true', '0', 'false'),
        }),
        async (data) => {
          expect.hasAssertions();

          const actual = await transformAndValidate(MoviePatch, data);

          ((expect(
            actual.availability,
          ) as unknown) as ExtendedMatchers).toBeBoolean();
        },
      ),
    ));

  it('should fail to validate the patch', () =>
    fc.assert(
      fc.asyncProperty(
        fc.record({
          availability: fc.string(),
        }),
        async (data) => {
          fc.pre(!['1', 'true', '0', 'false'].includes(data.availability));

          await expect(
            transformAndValidate(MoviePatch, data),
          ).rejects.toBeDefined();
        },
      ),
    ));
});
