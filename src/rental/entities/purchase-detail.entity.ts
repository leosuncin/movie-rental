import { Entity, PrimaryGeneratedColumn, ManyToOne, Column } from 'typeorm';
import { Movie } from './movie.entity';
import { Purchase } from './purchase.entity';

@Entity()
export class PurchaseDetail {
  @PrimaryGeneratedColumn('uuid')
  id!: string;

  @ManyToOne(type => Movie, { nullable: false })
  movie!: Movie;

  @Column('integer', { default: 1 })
  quantity = 1;

  @Column('decimal')
  price!: number;

  @ManyToOne(type => Purchase, purchase => purchase.details, {
    nullable: false,
  })
  purchase!: Purchase;
}
