import slugify from 'slugify';
import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  BeforeInsert,
  ManyToMany,
  JoinTable,
} from 'typeorm';

import { MovieRate } from '../movie-rate.enum';
import { User } from '../../auth/user.entity';
import { Exclude } from 'class-transformer';

type CriticSource =
  | 'Internet Movie Database'
  | 'Rotten Tomatoes'
  | 'Metacritic';
interface Rate {
  source: CriticSource;
  value: string;
}

@Entity()
export class Movie {
  @PrimaryGeneratedColumn('uuid')
  id!: string;

  @Column()
  title!: string;

  @Column()
  description!: string;

  @Column('simple-array')
  images!: string[];

  @Column({ type: 'integer', default: 0 })
  stock = 0;

  @Column('decimal')
  rentalPrice!: number;

  @Column('decimal')
  salePrice!: number;

  @Column({ type: 'boolean', default: false })
  availability = false;

  @Column({ unique: true })
  slug!: string;

  @Column()
  year!: number;

  @Column({ enum: MovieRate })
  rated!: MovieRate;

  @Column()
  releaseDate!: Date;

  @Column()
  runtime!: string;

  @Column('simple-array')
  genre!: string[];

  @Column()
  director!: string;

  @Column('simple-array')
  writers!: string[];

  @Column('simple-array')
  actors!: string[];

  @Column({ nullable: true })
  production?: string;

  @Column('simple-json')
  ratings!: Rate[];

  @ManyToMany(type => User, user => user.moviesLiked)
  @JoinTable()
  @Exclude()
  likedBy!: User[];

  @Column({ default: 0 })
  readonly likes!: number;

  @BeforeInsert()
  setSlug(title?: string) {
    this.slug = slugify(title || this.title, { lower: true }) + '-' + this.year;
  }
}
