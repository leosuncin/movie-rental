import { transformAndValidateSync } from 'class-transformer-validator';

import { Purchase } from './purchase.entity';
import { PurchaseDetail, PurchaseMovies } from '../dto/purchase-movies.dto';

describe('Purchase', () => {
  it('should be defined', () => {
    expect(new Purchase()).toBeDefined();
    expect(new PurchaseDetail()).toBeDefined();
  });
});

describe('Purchases movies DTO', () => {
  it('should be defined', () => {
    expect(new PurchaseMovies()).toBeDefined();
  });

  it('should validate to purchase a movie', () => {
    const data = {
      details: [
        {
          movie: '3a60114b-89d4-4156-9453-09b8c5b9b42c',
          quantity: '1',
          price: '10.0',
        },
      ],
    };

    expect(transformAndValidateSync(PurchaseMovies, data)).toBeDefined();
  });
});
