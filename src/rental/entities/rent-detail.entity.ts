import { Entity, PrimaryGeneratedColumn, ManyToOne, Column } from 'typeorm';

import { Movie } from './movie.entity';
import { Rent } from './rent.entity';

@Entity()
export class RentDetail {
  @PrimaryGeneratedColumn('uuid')
  id!: string;

  @ManyToOne(type => Movie, { nullable: false })
  movie!: Movie;

  @Column('integer', { default: 1 })
  quantity = 1;

  @Column('decimal')
  price!: number;

  @ManyToOne(type => Rent, rent => rent.details, { nullable: false })
  rent!: Rent;
}
