import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  ManyToOne,
  OneToMany,
  BeforeInsert,
} from 'typeorm';

import { User } from '../../auth/user.entity';
import { PurchaseDetail } from './purchase-detail.entity';

@Entity()
export class Purchase {
  @PrimaryGeneratedColumn('uuid')
  id!: string;

  @Column('decimal')
  total!: number;

  @Column({ default: () => 'CURRENT_TIMESTAMP' })
  purchasedAt: Date = new Date();

  @ManyToOne(type => User, { nullable: false })
  client!: User;

  @OneToMany(type => PurchaseDetail, detail => detail.purchase, {
    cascade: true,
  })
  details!: PurchaseDetail[];

  @BeforeInsert()
  private beforeSave() {
    this.total = this.details.reduce(
      (prev: number, curr: PurchaseDetail) => prev + curr.price * curr.quantity,
      0,
    );
  }
}
