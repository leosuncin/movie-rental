/// <reference types="../global" />
import { Logger } from '@nestjs/common';
import {
  EventSubscriber,
  EntitySubscriberInterface,
  UpdateEvent,
} from 'typeorm';
import diff from 'diffler';

import { Movie } from '../rental/entities/movie.entity';

@EventSubscriber()
export class MovieSubscriber implements EntitySubscriberInterface<Movie> {
  listenTo() {
    return Movie;
  }

  afterUpdate(event: UpdateEvent<Movie>) {
    const differences = diff(event.databaseEntity, event.entity);
    if (!this.isEmpty(differences))
      Logger.verbose(differences, 'MovieLogger', false);
  }

  isEmpty(obj: object): boolean {
    return Object.getOwnPropertyNames(obj).length === 0;
  }
}
