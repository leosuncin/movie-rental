import amqp from 'amqplib';
import sendgrid from '@sendgrid/mail';
import { MailDataRequired } from '@sendgrid/helpers/classes/mail';
import { Logger } from '@nestjs/common';

import { QUEUE_NAME } from '../constants';

(async () => {
  sendgrid.setApiKey(process.env.SENDGRID_API_KEY as string);
  const connection = await amqp.connect(process.env.CLOUDAMQP_URL as string);
  const channel = await connection.createChannel();

  await channel.assertQueue(QUEUE_NAME, { durable: true });
  await channel.prefetch(1);

  channel.consume(
    QUEUE_NAME,
    async (msg) => {
      if (msg) {
        const payload: MailDataRequired = JSON.parse(msg.content.toString());
        try {
          const [response] = await sendgrid.send(payload);
          if (response.statusCode >= 200 && response.statusCode < 300)
            channel.ack(msg);
          else Logger.debug(response, 'EmailWorker');
        } catch (err) {
          Logger.error(
            `Error sending email to ${payload.to}`,
            err,
            'EmailWorker',
            false,
          );
        }
      }
    },
    { noAck: false },
  );

  function handleExit() {
    channel.close();
    connection.close();
  }

  process.on('exit', handleExit);
  process.on('SIGINT', handleExit);
})();
