import {
  Injectable,
  Logger,
  ServiceUnavailableException,
} from '@nestjs/common';
import { MailData } from '@sendgrid/helpers/classes/mail';
import { Connection, Channel } from 'amqplib';
import Mailgen from 'mailgen';
import { InjectAmqpConnection } from 'nestjs-amqp';

import { User } from '../auth/user.entity';
import { QUEUE_NAME } from '../constants';

@Injectable()
export class EmailService {
  private channel: Channel | undefined;
  private mailGen: Mailgen;

  constructor(
    @InjectAmqpConnection()
    private readonly amqpConnection: Connection,
  ) {
    amqpConnection
      .createChannel()
      .then(channel => {
        this.channel = channel;
        return this.channel.assertQueue(QUEUE_NAME);
      })
      .catch(err => Logger.error(err.message, err, 'EmailService', false));
    this.mailGen = new Mailgen({
      theme: 'salted',
      product: {
        name: 'Movie Rental',
        link: process.env.BASE_URL as string,
      },
    });
  }

  sendWelcomeEmail(user: User, link: string) {
    const email: Mailgen.Content = {
      body: {
        name: user.name,
        intro:
          "Welcome to Movie Rental! We're very excited to have you on board.",
        action: {
          instructions: 'To get started with Movie Rental, please click here:',
          button: {
            color: '#22BC66',
            text: 'Confirm your account',
            link,
          },
        },
      },
    };
    const payload: MailData = {
      from: 'noreply@mov-rent.herokuapp.com',
      to: user.email,
      subject: 'Confirm account',
      text: this.mailGen.generatePlaintext(email),
      html: this.mailGen.generate(email),
    };

    return this.sendEmail(payload);
  }

  sendRecoveryEmail(user: User, link: string) {
    const email: Mailgen.Content = {
      body: {
        name: user.name,
        intro:
          'You have received this email because a password reset request for your account was received.',
        action: {
          instructions: 'Click the button below to reset your password:',
          button: {
            color: '#DC4D2F',
            text: 'Reset your password',
            link,
          },
        },
        outro:
          'If you did not request a password reset, no further action is required on your part.',
      },
    };
    const payload: MailData = {
      from: 'noreply@mov-rent.herokuapp.com',
      to: user.email,
      subject: 'Confirm account',
      text: this.mailGen.generatePlaintext(email),
      html: this.mailGen.generate(email),
    };

    return this.sendEmail(payload);
  }

  passwordChangedEmail(user: User) {
    const email: Mailgen.Content = {
      body: {
        name: user.name,
        intro: 'Your password was changed successfully.',
        outro: 'We hope now you can sign in without troubles.',
      },
    };
    const payload: MailData = {
      from: 'noreply@mov-rent.herokuapp.com',
      to: user.email,
      subject: 'Password change',
      text: this.mailGen.generatePlaintext(email),
      html: this.mailGen.generate(email),
    };

    return this.sendEmail(payload);
  }

  private sendEmail(payload: MailData) {
    if (!this.channel)
      throw new ServiceUnavailableException(
        `Channel for queue «${QUEUE_NAME}» is not ready`,
      );

    this.channel.sendToQueue(QUEUE_NAME, Buffer.from(JSON.stringify(payload)), {
      persistent: true,
    });
  }
}
