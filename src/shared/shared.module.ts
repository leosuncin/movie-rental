import { Module } from '@nestjs/common';
import { AmqpModule } from 'nestjs-amqp';
import url from 'url';

import { EmailService } from './email.service';

const { hostname, port, pathname: vhost, auth = '' } = url.parse(
  process.env.CLOUDAMQP_URL as string,
);
const [username, password] = (auth || '').split(':');

@Module({
  imports: [
    AmqpModule.forRoot({
      hostname: hostname || '',
      ...(port ? { port: parseInt(port, 10) } : {}),
      username,
      password,
      vhost: vhost || '',
    }),
  ],
  providers: [EmailService],
  exports: [EmailService],
})
export class SharedModule {}
