import { transformAndValidate } from 'class-transformer-validator';
import fc from 'fast-check';
import faker from 'faker';

import { User } from './user.entity';
import { UserRegister } from './dtos/user-register.dto';
import { UserLogin } from './dtos/user-login.dto';
import { UserRecover } from './dtos/user-recover.dto';
import { UserRestore } from './dtos/user-restore.dto';

const fakerToArbitrary = <F>(cb: (seed: number) => F) =>
  fc
    .integer()
    .noBias()
    .noShrink()
    .map<F>(cb);

describe('user entity', () => {
  it('should be defined', () => {
    expect(new User()).toBeDefined();
  });
});

describe('register DTO', () => {
  const registerArbitrary = () =>
    fakerToArbitrary<UserRegister>(seed => {
      faker.seed(seed);

      const firstName = faker.name.firstName();
      const lastName = faker.name.lastName();
      const email = faker.internet.exampleEmail(firstName, lastName);
      const password = faker.internet.password(8);

      return {
        name: `${firstName} ${lastName}`,
        email,
        password,
      };
    });

  it('should be defined', () => {
    expect(new UserRegister()).toBeDefined();
  });

  it('should be validated', () =>
    fc.assert(
      fc.asyncProperty<UserRegister>(registerArbitrary(), async data => {
        expect.hasAssertions();
        const actual = await transformAndValidate(UserRegister, data);

        expect(actual).toBeDefined();
      }),
    ));

  it('should require the name', async () => {
    expect.assertions(3);
    const data = {
      email: faker.internet.exampleEmail(),
      password: faker.internet.password(8),
    };

    await expect(
      transformAndValidate(UserRegister, data),
    ).rejects.toBeDefined();
    await expect(
      transformAndValidate(UserRegister, { ...data, name: 123456 }),
    ).rejects.toBeDefined();
    await expect(
      transformAndValidate(UserRegister, { ...data, name: '' }),
    ).rejects.toBeDefined();
  });

  it('should require the email', async () => {
    expect.assertions(4);
    const data = {
      name: faker.fake('{{name.firstName}} {{name.lastName}}'),
      password: faker.internet.password(8),
    };

    await expect(
      transformAndValidate(UserRegister, data),
    ).rejects.toBeDefined();
    await expect(
      transformAndValidate(UserRegister, { ...data, email: 123456 }),
    ).rejects.toBeDefined();
    await expect(
      transformAndValidate(UserRegister, { ...data, email: '' }),
    ).rejects.toBeDefined();
    await expect(
      transformAndValidate(UserRegister, {
        ...data,
        email: faker.internet.userName(),
      }),
    ).rejects.toBeDefined();
  });

  it('should require the password', async () => {
    expect.assertions(4);
    const data = {
      name: faker.fake('{{name.firstName}} {{name.lastName}}'),
      email: faker.internet.exampleEmail(),
    };

    await expect(
      transformAndValidate(UserRegister, data),
    ).rejects.toBeDefined();
    await expect(
      transformAndValidate(UserRegister, { ...data, password: 123456 }),
    ).rejects.toBeDefined();
    await expect(
      transformAndValidate(UserRegister, { ...data, password: '' }),
    ).rejects.toBeDefined();
    await expect(
      transformAndValidate(UserRegister, {
        ...data,
        password: faker.internet.password(7),
      }),
    ).rejects.toBeDefined();
  });
});

describe('login DTO', () => {
  const loginArbitrary = () =>
    fakerToArbitrary<UserLogin>(seed => {
      faker.seed(seed);

      const email = faker.internet.exampleEmail();
      const password = faker.internet.password(8);

      return ({ email, password } as unknown) as UserLogin;
    });

  it('should be defined', () => {
    expect(new UserLogin('', '')).toBeDefined();
  });

  it('should validate login DTO', () =>
    fc.assert(
      fc.asyncProperty<UserLogin>(loginArbitrary(), async (data: UserLogin) => {
        expect.hasAssertions();
        const actual = await transformAndValidate(UserLogin, data);

        expect(actual).toBeDefined();
      }),
    ));

  it('should require the email', async () => {
    expect.assertions(4);
    const data = {
      name: faker.fake('{{name.firstName}} {{name.lastName}}'),
      password: faker.internet.password(8),
    };

    await expect(transformAndValidate(UserLogin, data)).rejects.toBeDefined();
    await expect(
      transformAndValidate(UserLogin, { ...data, email: 123456 }),
    ).rejects.toBeDefined();
    await expect(
      transformAndValidate(UserLogin, { ...data, email: '' }),
    ).rejects.toBeDefined();
    await expect(
      transformAndValidate(UserLogin, {
        ...data,
        email: faker.internet.userName(),
      }),
    ).rejects.toBeDefined();
  });

  it('should require the password', async () => {
    expect.assertions(4);
    const data = {
      name: faker.fake('{{name.firstName}} {{name.lastName}}'),
      email: faker.internet.exampleEmail(),
    };

    await expect(transformAndValidate(UserLogin, data)).rejects.toBeDefined();
    await expect(
      transformAndValidate(UserLogin, { ...data, password: 123456 }),
    ).rejects.toBeDefined();
    await expect(
      transformAndValidate(UserLogin, { ...data, password: '' }),
    ).rejects.toBeDefined();
    await expect(
      transformAndValidate(UserLogin, {
        ...data,
        password: faker.internet.password(7),
      }),
    ).rejects.toBeDefined();
  });
});

describe('Recover password DTO', () => {
  it('should be defined', () => expect(new UserRecover()).toBeDefined());
});

describe('Restore password DTO', () => {
  it('should be defined', () => expect(new UserRestore()).toBeDefined());

  it('should validate repeat passwords', () =>
    fc.assert(
      fc.asyncProperty(fc.string(8, 100), async password => {
        expect.hasAssertions();
        const actual = await transformAndValidate(UserRestore, {
          newPassword: password,
          repeatPassword: password,
        });

        expect(actual).toBeDefined();
      }),
    ));
});
