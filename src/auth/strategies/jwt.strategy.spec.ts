import { Test, TestingModule } from '@nestjs/testing';
import { getRepositoryToken } from '@nestjs/typeorm';
import httpMocks from 'node-mocks-http';

import { JwtStrategy, extractJwtFromCookie } from './jwt.strategy';
import { AuthService } from '../auth.service';
import { User } from '../user.entity';
import { EmailService } from '../../shared/email.service';

describe('JwtStrategy', () => {
  let strategy: JwtStrategy;
  const findOneMock = jest.fn(dto =>
    Promise.resolve(
      dto.id !== '00000000-0000-0000-0000-000000000000'
        ? Object.assign(new User(), dto)
        : null,
    ),
  );
  const signMock = jest.fn(dto =>
    Buffer.from(JSON.stringify(dto)).toString('base64'),
  );

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        JwtStrategy,
        AuthService,
        {
          provide: getRepositoryToken(User),
          useValue: {
            findOne: findOneMock,
          },
        },
        { provide: 'JwtService', useValue: { sign: signMock } },
        EmailService,
        {
          provide: 'AMQP_CONNECTION_PROVIDER_default',
          useValue: {
            createChannel() {
              return Promise.resolve({
                assertQueue: jest.fn(),
                sendToQueue: jest.fn(),
              });
            },
          },
        },
      ],
    }).compile();

    strategy = module.get<JwtStrategy>(JwtStrategy);
  });

  afterEach(() => {
    findOneMock.mockClear();
    signMock.mockClear();
  });

  it('should be defined', () => {
    expect(strategy).toBeDefined();
  });

  it('should validate the payload', async () => {
    expect.hasAssertions();
    const payload = {
      sub: '00000000-0000-0000-0000-000000000001',
      roles: [],
      iat: Math.random() * 1e4,
      exp: Math.random() * 1e4,
    };
    const user = await strategy.validate(payload);

    expect(user).toBeDefined();
  });

  it('should fail to validate the payload', () => {
    expect.hasAssertions();
    const payload = {
      sub: '00000000-0000-0000-0000-000000000000',
      roles: [],
      iat: Math.random() * 1e4,
      exp: Math.random() * 1e4,
    };

    return expect(strategy.validate(payload)).rejects.toThrow();
  });
});

describe('extractJwtFromCookie', () => {
  it('should extract token from default cookie', () => {
    const req = httpMocks.createRequest({
      cookies: {
        token: 'mock.jwt.token',
      },
      signedCookies: {
        token: 'mock.jwt.token',
      },
    });

    expect(extractJwtFromCookie()(req)).toBe('mock.jwt.token');
  });

  it('should extract token from custom cookie', () => {
    const cookieName = 'jwt_token';
    const req = httpMocks.createRequest({
      cookies: {
        [cookieName]: 'mock.jwt.token',
      },
      signedCookies: {
        [cookieName]: 'mock.jwt.token',
      },
    });

    expect(extractJwtFromCookie(cookieName)(req)).toBe('mock.jwt.token');
  });
});
