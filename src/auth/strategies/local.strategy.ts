import { Injectable, BadRequestException } from '@nestjs/common';
import { PassportStrategy } from '@nestjs/passport';
import { validateOrReject } from 'class-validator';
import { Strategy } from 'passport-local';

import { AuthService } from '../auth.service';
import { User } from '../user.entity';
import { UserLogin } from '../dtos/user-login.dto';

@Injectable()
export class LocalStrategy extends PassportStrategy(Strategy, 'local') {
  constructor(private readonly authService: AuthService) {
    super({
      usernameField: 'email',
      passReqToCallback: false,
    });
  }

  async validate(email: string, password: string): Promise<User> {
    try {
      await validateOrReject(new UserLogin(email, password), {
        validationError: { target: false },
      });
    } catch (errors) {
      throw new BadRequestException(errors);
    }

    const user = await this.authService.verifyCredentials(email, password);

    delete user.password;

    return user;
  }
}
