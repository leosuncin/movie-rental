import { Injectable } from '@nestjs/common';
import { PassportStrategy } from '@nestjs/passport';
import { Request } from 'express';
import { Strategy, ExtractJwt } from 'passport-jwt';

import { AuthService } from '../auth.service';
import { JwtPayload } from '../jwt-payload.interface';

export const extractJwtFromCookie = (cookieName = 'token') => {
  return (req: Request) => {
    let token = '';
    if (req.cookies && cookieName in req.cookies)
      token = req.cookies[cookieName];
    if (req.signedCookies && cookieName in req.signedCookies)
      token = req.signedCookies[cookieName];
    return token;
  };
};

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
  constructor(private readonly authService: AuthService) {
    super({
      jwtFromRequest: ExtractJwt.fromExtractors([
        ExtractJwt.fromAuthHeaderAsBearerToken(),
        extractJwtFromCookie(),
      ]),
      secretOrKey: process.env.APP_SECRET,
      passReqToCallback: false,
    });
  }

  validate(payload: JwtPayload) {
    return this.authService.verifyPayload(payload);
  }
}
