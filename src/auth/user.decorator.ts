import { createParamDecorator } from '@nestjs/common';
import { Request } from 'express';

import { User } from './user.entity';

export const AuthUser = createParamDecorator(
  (data: keyof User, req: Request & {}) => {
    const user = (req.user || req.session!.passport
      ? req.session!.passport.user
      : null) as User;
    return data ? user && user[data] : user;
  },
);
