import { Test, TestingModule } from '@nestjs/testing';
import { getRepositoryToken } from '@nestjs/typeorm';
import httpMocks from 'node-mocks-http';

import { AuthController } from './auth.controller';
import { AuthService } from './auth.service';
import { User } from './user.entity';
import { EmailService } from '../shared/email.service';

const tomorrow = 24 * 3600 * 1e3;

describe('AuthController', () => {
  let controller: AuthController;
  const findOneMock = jest.fn((dto) =>
    Promise.resolve(
      dto.email !== 'none@email.com' && dto.id !== ''
        ? Object.assign(new User(), dto, {
            password: 'dr0w$$4P',
            updatedAt: new Date(Date.now() - tomorrow),
          })
        : null,
    ),
  );
  const saveMock = jest.fn((dto) =>
    Promise.resolve(Object.assign(new User(), dto)),
  );
  const countMock = jest.fn((dto) => (dto.email !== 'none@email.com' ? 0 : 1));
  const signMock = jest.fn((dto) =>
    Buffer.from(JSON.stringify(dto)).toString('base64'),
  );

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [AuthController],
      providers: [
        AuthService,
        {
          provide: getRepositoryToken(User),
          useValue: {
            findOne: findOneMock,
            save: saveMock,
            count: countMock,
          },
        },
        {
          provide: 'JwtService',
          useValue: {
            sign: signMock,
            decode(token: string): { sub: string } {
              return { sub: token };
            },
          },
        },
        EmailService,
        {
          provide: 'AMQP_CONNECTION_PROVIDER_default',
          useValue: {
            createChannel(): Promise<{
              assertQueue: jest.Mock;
              sendToQueue: jest.Mock;
            }> {
              return Promise.resolve({
                assertQueue: jest.fn(),
                sendToQueue: jest.fn(),
              });
            },
          },
        },
      ],
    }).compile();

    controller = module.get<AuthController>(AuthController);
  });

  afterEach(() => {
    findOneMock.mockClear();
    saveMock.mockClear();
    countMock.mockClear();
    signMock.mockClear();
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  it('should register an user', async () => {
    expect.assertions(4);
    const data = {
      name: 'John Doe',
      email: 'john@doe.me',
      password: 'P4$$w0rd',
    };
    const resp = httpMocks.createResponse();

    await controller.register(data, resp);

    expect(saveMock).toHaveBeenCalled();
    expect(signMock).toHaveBeenCalled();
    expect(resp._getData()).not.toHaveProperty('password');
    expect(resp.cookies).toHaveProperty('token');
  });

  it('should fail to register an user', () => {
    expect.hasAssertions();
    const data = {
      name: 'John Doe',
      email: 'none@email.com',
      password: 'P4$$w0rd',
    };
    const resp = httpMocks.createResponse();

    return expect(controller.register(data, resp)).rejects.toThrow();
  });

  it('should login an user', () => {
    expect.assertions(3);
    const req = httpMocks.createRequest({
      user: {
        id: 'ecdcf8a4-c594-407a-a8f9-78c8e89b1d20',
      },
    });
    const resp = httpMocks.createResponse();

    controller.login(req, resp);

    expect(signMock).toHaveBeenCalled();
    expect(resp._getData()).not.toHaveProperty('password');
    expect(resp.cookies).toHaveProperty('token');
  });

  it('should confirm a pending user', () =>
    expect(
      controller.confirm('ecdcf8a4-c594-407a-a8f9-78c8e89b1d20'),
    ).resolves.toBeDefined());

  it("should fail to confirm if user doesn't exists", () =>
    expect(controller.confirm('')).rejects.toThrow());

  it('should request a recovery password', () =>
    expect(
      controller.recover({ email: 'john@doe.me' }),
    ).resolves.toBeDefined());

  it('should fail to recover not existing user', () =>
    expect(controller.recover({ email: 'none@email.com' })).rejects.toThrow());

  it('should fail due many request', async () => {
    expect.assertions(1);
    const now = Date.now();
    const spy = jest
      .spyOn(Date, 'now')
      .mockImplementationOnce(() => now + tomorrow + 1)
      .mockImplementationOnce(() => now);

    await expect(
      controller.recover({ email: 'john@doe.me' }),
    ).rejects.toThrow();

    spy.mockClear();
  });

  it('should restore the password of an user', () =>
    expect(
      controller.restore('john@doe.me', {
        newPassword: 'P4$$word',
        repeatPassword: 'P4$$word',
      }),
    ).resolves.toBeDefined());
});
