import {
  Injectable,
  UnprocessableEntityException,
  UnauthorizedException,
  GoneException,
  NotFoundException,
  HttpException,
} from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

import { User } from './user.entity';
import { UserRegister } from './dtos/user-register.dto';
import { JwtPayload } from './jwt-payload.interface';
import { EmailService } from '../shared/email.service';
import { UserRestore } from './dtos/user-restore.dto';

@Injectable()
export class AuthService {
  constructor(
    @InjectRepository(User)
    private readonly userRepository: Repository<User>,
    private readonly jwtService: JwtService,
    private readonly emailService: EmailService,
  ) {}

  async register(newUser: UserRegister): Promise<User> {
    if ((await this.userRepository.count({ email: newUser.email })) > 0) {
      throw new UnprocessableEntityException(
        `The email «${newUser.email}» is already register.`,
      );
    }

    const user = Object.assign(new User(), newUser, { roles: ['USER_ROLE'] });

    return this.userRepository.save(user);
  }

  async verifyCredentials(email: string, password: string) {
    const user = await this.userRepository.findOne({ email });

    if (!user)
      throw new UnauthorizedException(
        `There isn't any user with email: ${email}`,
      );

    if (!(await user.checkPassword(password)))
      throw new UnauthorizedException(
        `Wrong password for user with email: ${email}`,
      );

    return user;
  }

  async verifyPayload(payload: JwtPayload) {
    const user = await this.userRepository.findOne({ id: payload.sub });

    if (!user)
      throw new UnauthorizedException(
        `There isn't any user with id: ${payload.sub}`,
      );

    return user;
  }

  signToken(user: User) {
    const payload = {
      sub: user.id,
      roles: user.roles,
    };

    return this.jwtService.sign(payload);
  }

  async confirm(id: string) {
    const user = await this.userRepository.findOne({ id, pending: true });

    if (!user)
      throw new GoneException(`User does not exists or already confirmed`);

    user.pending = false;

    return this.userRepository.save(user);
  }

  async recoverPassword(email: string) {
    const user = await this.userRepository.findOne({ email });

    if (!user)
      throw new NotFoundException(`There isn't any user with email: ${email}`);

    if (Date.now() - user.updatedAt.getTime() < 24 * 3600 * 1e3)
      throw new HttpException(`Too many requests in less than 24 hours`, 429);

    const payload = {
      sub: user.email,
      type: 'recover_password',
    };
    const token = this.jwtService.sign(payload, { expiresIn: '1 day' });

    this.emailService.sendRecoveryEmail(
      user,
      `${process.env.BASE_URL}/api/auth/restore/${token}`,
    );
    user.updatedAt = new Date();

    await this.userRepository.save(user);

    return {
      message: `An email has been sent to ${user.email} with instructions`,
      status: 202,
      statusCode: 'ACCEPTED',
    };
  }

  async restorePassword(token: string, restore: UserRestore) {
    const payload = this.jwtService.decode(token) as JwtPayload;

    if (!payload) throw new UnprocessableEntityException('Token invalid');

    const user = await this.userRepository.findOne({ email: payload.sub });

    if (!user)
      throw new NotFoundException(`There isn't any user with that token`);

    user.updatePassword(restore.newPassword);

    return this.userRepository.save(user);
  }
}
