import { ExecutionContext } from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import faker from 'faker';
import fc from 'fast-check';

import { RolesGuard } from './roles.guard';

describe('AdminGuard', () => {
  it('should be defined', () => {
    const reflector = {
      get() {
        return [];
      },
    };
    expect(new RolesGuard((reflector as unknown) as Reflector)).toBeDefined();
  });

  it('should authorize', () => {
    const reflector = {
      get() {
        return ['ADMIN_ROLE'];
      },
    };
    const context = {
      getClass() {
        return {};
      },
      switchToHttp() {
        return {
          getRequest() {
            return {
              user: {
                roles: ['USER_ROLE', 'ADMIN_ROLE'],
              },
            };
          },
        };
      },
    };

    expect(
      new RolesGuard((reflector as unknown) as Reflector).canActivate(
        context as ExecutionContext,
      ),
    ).toBe(true);
  });

  it('should authorize without roles', () => {
    const reflector = {
      get() {
        return [];
      },
    };
    const context = {
      getClass() {
        return {};
      },
    };

    expect(
      new RolesGuard((reflector as unknown) as Reflector).canActivate(
        context as ExecutionContext,
      ),
    ).toBe(true);
  });

  it('should not authorize user without required roles', () => {
    const reflector = {
      get() {
        return ['ADMIN_ROLE'];
      },
    };
    const context = {
      getClass() {
        return {};
      },
      switchToHttp() {
        return {
          getRequest() {
            return {
              user: {
                roles: ['USER_ROLE'],
              },
            };
          },
        };
      },
    };

    expect(
      new RolesGuard((reflector as unknown) as Reflector).canActivate(
        context as ExecutionContext,
      ),
    ).toBe(false);
  });

  it('should not authorize anonymous user', () => {
    const reflector = {
      get() {
        return ['ADMIN_ROLE'];
      },
    };
    const context = {
      getClass() {
        return {};
      },
      switchToHttp() {
        return {
          getRequest() {
            return {
              user: null,
            };
          },
        };
      },
    };

    expect(
      new RolesGuard((reflector as unknown) as Reflector).canActivate(
        context as ExecutionContext,
      ),
    ).toBe(false);
  });

  it('should not authorize user without roles', () => {
    const reflector = {
      get() {
        return ['ADMIN_ROLE', 'USER_ROLE'];
      },
    };
    const context = {
      getClass() {
        return {};
      },
      switchToHttp() {
        return {
          getRequest() {
            return {
              user: {
                roles: [],
              },
            };
          },
        };
      },
    };

    expect(
      new RolesGuard((reflector as unknown) as Reflector).canActivate(
        context as ExecutionContext,
      ),
    ).toBe(false);
  });

  it('should authorize with at least one role', () =>
    fc.assert(
      fc.property(
        fc.subarray(['ADMIN_ROLE', 'USER_ROLE', 'SALE_ROLE']),
        (roles) => {
          fc.pre(roles.length > 1);

          const reflector = {
            get() {
              return roles;
            },
          };
          const context = {
            getClass() {
              return {};
            },
            switchToHttp() {
              return {
                getRequest() {
                  return {
                    user: {
                      roles: [faker.random.arrayElement(roles)],
                    },
                  };
                },
              };
            },
          };

          return new RolesGuard(
            (reflector as unknown) as Reflector,
          ).canActivate(context as ExecutionContext);
        },
      ),
    ));
});
