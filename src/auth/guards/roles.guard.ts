import { Injectable, CanActivate, ExecutionContext } from '@nestjs/common';
import { Reflector } from '@nestjs/core';

import { User } from '../user.entity';

@Injectable()
export class RolesGuard implements CanActivate {
  constructor(private readonly reflector: Reflector) {}

  canActivate(context: ExecutionContext): boolean {
    const roles = this.reflector.get<string[]>('roles', context.getClass());

    if (!roles || !roles.length) {
      return true;
    }

    const user = context.switchToHttp().getRequest().user as User;

    if (!user) return false;

    return user.roles.some(role => roles.includes(role));
  }
}
