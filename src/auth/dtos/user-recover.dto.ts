import { IsDefined, IsEmail } from 'class-validator';

export class UserRecover {
  @IsDefined()
  @IsEmail()
  readonly email!: string;
}
