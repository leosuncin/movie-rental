import { IsDefined, IsString, IsNotEmpty, MinLength } from 'class-validator';
import { repeatValue } from '../../shared/repeat-value.validator';

export class UserRestore {
  @IsDefined()
  @IsString()
  @IsNotEmpty()
  @MinLength(8)
  newPassword!: string;

  @IsDefined()
  @IsString()
  @IsNotEmpty()
  @MinLength(8)
  @repeatValue('newPassword')
  repeatPassword!: string;
}
