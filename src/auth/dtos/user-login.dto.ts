import {
  IsDefined,
  IsNotEmpty,
  IsEmail,
  MinLength,
  IsString,
} from 'class-validator';

export class UserLogin {
  @IsDefined()
  @IsString()
  @IsEmail()
  private readonly email: string;

  @IsDefined()
  @IsString()
  @IsNotEmpty()
  @MinLength(8)
  private readonly password: string;

  constructor(email: string, password: string) {
    this.email = email;
    this.password = password;
  }
}
