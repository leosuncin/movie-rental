import {
  IsDefined,
  IsNotEmpty,
  IsEmail,
  MinLength,
  IsString,
} from 'class-validator';

export class UserRegister {
  @IsDefined()
  @IsString()
  @IsNotEmpty()
  readonly name!: string;

  @IsDefined()
  @IsEmail()
  readonly email!: string;

  @IsDefined()
  @IsString()
  @IsNotEmpty()
  @MinLength(8)
  readonly password!: string;
}
