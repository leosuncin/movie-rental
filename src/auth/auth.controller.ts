import {
  Body,
  Controller,
  HttpCode,
  HttpStatus,
  Post,
  Req,
  Res,
  UseGuards,
  Get,
  Param,
} from '@nestjs/common';
import { Response, Request } from 'express';

import { AuthService } from './auth.service';
import { UserRegister } from './dtos/user-register.dto';
import { User } from './user.entity';
import { UserRecover } from './dtos/user-recover.dto';
import { UserRestore } from './dtos/user-restore.dto';
import { LocalAuthGuard } from './guards/local-auth.guard';

@Controller('api/auth')
export class AuthController {
  constructor(private readonly authService: AuthService) {}

  @Post('register')
  @HttpCode(HttpStatus.ACCEPTED)
  async register(@Body() newUser: UserRegister, @Res() resp: Response) {
    const user = await this.authService.register(newUser);
    const token = this.authService.signToken(user);

    delete user.password;
    resp.cookie('token', token, { httpOnly: true, signed: true });
    resp.send(user);

    return resp;
  }

  @Post('login')
  @UseGuards(LocalAuthGuard)
  @HttpCode(HttpStatus.OK)
  login(@Req() req: Request, @Res() resp: Response) {
    const user = req.user as User;
    const token = this.authService.signToken(user);

    delete user.password;
    resp.cookie('token', token, { httpOnly: true, signed: true });
    resp.send(user);

    return resp;
  }

  @Get(
    'confirm/:id([0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12})',
  )
  confirm(@Param('id') id: string) {
    return this.authService.confirm(id);
  }

  @Post('/recover')
  @HttpCode(HttpStatus.ACCEPTED)
  recover(@Body() recoverReq: UserRecover) {
    return this.authService.recoverPassword(recoverReq.email);
  }

  @Post('/restore/:token(w+.w+.w+.w+)')
  @HttpCode(HttpStatus.NO_CONTENT)
  restore(@Param('token') token: string, @Body() restore: UserRestore) {
    return this.authService.restorePassword(token, restore);
  }
}
