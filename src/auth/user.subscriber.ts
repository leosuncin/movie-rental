import { Injectable } from '@nestjs/common';
import { InjectConnection } from '@nestjs/typeorm';
import {
  EntitySubscriberInterface,
  Connection,
  InsertEvent,
  UpdateEvent,
} from 'typeorm';

import { User } from './user.entity';
import { EmailService } from '../shared/email.service';

@Injectable()
export class UserSubscriber implements EntitySubscriberInterface<User> {
  constructor(
    @InjectConnection()
    private readonly connection: Connection,
    private readonly emailService: EmailService,
  ) {
    connection.subscribers.push(this);
  }

  listenTo() {
    return User;
  }

  afterInsert(event: InsertEvent<User>) {
    const user = event.entity;
    const link = `${process.env.BASE_URL}/api/auth/confirm/${user.id}`;

    this.emailService.sendWelcomeEmail(user, link);
  }

  afterUpdate(event: UpdateEvent<User>) {
    if (event.databaseEntity.password !== event.entity.password) {
      this.emailService.passwordChangedEmail(event.entity);
    }
  }
}
