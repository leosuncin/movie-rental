import { MigrationInterface, QueryRunner } from 'typeorm';

export class AddPendingColumn2User1569600430039 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(
      `ALTER TABLE "user" ADD "pending" boolean NOT NULL DEFAULT true`,
    );
    await queryRunner.query(
      `UPDATE "user" SET "pending" = false WHERE "createdAt" < CURRENT_TIMESTAMP`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(`ALTER TABLE "user" DROP COLUMN "pending"`);
  }
}
