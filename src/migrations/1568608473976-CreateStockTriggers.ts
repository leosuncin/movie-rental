import { MigrationInterface, QueryRunner } from 'typeorm';

export class CreateStockTriggers1568608473976 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(`CREATE FUNCTION decrement_movie_stock()
    RETURNS TRIGGER
    LANGUAGE plpgsql
    AS $$
      DECLARE
        "stock_available" integer := 0;
        "for_rental" boolean := TRUE;
      BEGIN
        SELECT "stock", "availability"
        INTO "stock_available", "for_rental"
        FROM "movie"
        WHERE
          "id" = NEW. "movieId";

        IF NOT "for_rental" THEN
          RAISE EXCEPTION 'Movie % is not for rental', NEW. "movieId";
        END IF;

        IF "stock_available" < NEW. "quantity" THEN
          RAISE EXCEPTION 'Stock available is %', "stock_available";
        ELSE
          UPDATE "movie"
          SET
            "stock" = "stock" - NEW. "quantity",
            "availability" = ("stock" - NEW. "quantity") > 0
          WHERE
            "id" = NEW. "movieId";
        END IF;

        RETURN NEW;
      END;
    $$`);
    await queryRunner.query(`CREATE TRIGGER "check_and_decrement_stock"
    BEFORE INSERT ON "rent_detail"
    FOR EACH ROW
    EXECUTE PROCEDURE decrement_movie_stock()`);
    await queryRunner.query(`CREATE FUNCTION increment_movie_stock()
    RETURNS TRIGGER
    LANGUAGE plpgsql
    AS $$
      DECLARE
        "details_cursor" CURSOR ("rentKey" uuid)
          FOR
            SELECT "movieId", "quantity"
            FROM "rent_detail"
            WHERE "rentId" = "rentKey";
        "details_record" RECORD;
      BEGIN
        IF NEW."returnedAt" IS NULL OR NEW."totalPaid" IS NULL THEN
          RETURN NEW;
        END IF;

        OPEN "details_cursor" (NEW."id");

        LOOP
          FETCH "details_cursor" INTO "details_record";
          EXIT WHEN NOT FOUND;

          UPDATE "movie"
          SET "stock" = "stock" + "details_record"."quantity"
          WHERE "id" = "details_record"."movieId";
        END LOOP;

        CLOSE "details_cursor";

        RETURN NEW;
      END;
    $$`);
    await queryRunner.query(`CREATE TRIGGER "increment_stock"
    AFTER UPDATE ON "rent"
    FOR EACH ROW
    EXECUTE PROCEDURE increment_movie_stock()`);
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(
      `DROP TRIGGER "check_and_decrement_stock" ON "rent_detail"`,
    );
    await queryRunner.query(`DROP FUNCTION "decrement_movie_stock"`);
    await queryRunner.query(`DROP TRIGGER "increment_stock" ON "rent"`);
    await queryRunner.query(`DROP FUNCTION "increment_movie_stock"`);
  }
}
