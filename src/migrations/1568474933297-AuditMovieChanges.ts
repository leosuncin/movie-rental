import { MigrationInterface, QueryRunner } from 'typeorm';

export class AuditMovieChanges1568474933297 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(`CREATE TABLE "movie_audit" (
        "id" uuid NOT NULL DEFAULT uuid_generate_v4(),
        "title" character varying NULL,
        "rentalPrice" numeric NULL,
        "salePrice" numeric NULL,
        "movieId" uuid NOT NULL,
        "createdAt" TIMESTAMP NOT NULL DEFAULT now(),
        "updatedAt" TIMESTAMP NOT NULL DEFAULT now(),
        PRIMARY KEY ("id"),
        FOREIGN KEY ("movieId")
          REFERENCES "movie"("id")
          ON DELETE CASCADE
          ON UPDATE NO ACTION
      )`);
    await queryRunner.query(`CREATE FUNCTION log_movie_changes()
        RETURNS trigger
        LANGUAGE plpgsql
        AS $$
          DECLARE
            "title" character varying;
            "rentalPrice" numeric;
            "salePrice" numeric;
          BEGIN
            IF OLD."title" <> NEW."title" THEN
              "title" := NEW."title";
              RAISE LOG 'Movie title change from % to %s', OLD."title", NEW."title";
            END IF;
            IF OLD."rentalPrice" <> NEW."rentalPrice" THEN
              "rentalPrice" := NEW."rentalPrice";
              RAISE LOG 'Movie rental price change from % to %s', OLD."rentalPrice", NEW."rentalPrice";
            END IF;
            IF OLD."salePrice" <> NEW."salePrice" THEN
              "salePrice" := NEW."salePrice";
              RAISE LOG 'Movie sale price change from % to %s', OLD."salePrice", NEW."salePrice";
            END IF;
            IF "title" IS NOT NULL OR "rentalPrice" IS NOT NULL OR "salePrice" IS NOT NULL THEN
              INSERT INTO "movie_audit" ("title", "rentalPrice", "salePrice", "movieId") VALUES ("title", "rentalPrice", "salePrice", NEW."id");
            END IF;

            RETURN NEW;
          END;
        $$`);
    await queryRunner.query(`CREATE TRIGGER "log_movie_update"
          AFTER UPDATE ON "movie"
          FOR EACH ROW
          EXECUTE PROCEDURE log_movie_changes()`);
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(`DROP TRIGGER "log_movie_update" ON "movie"`);
    await queryRunner.query(`DROP FUNCTION "log_movie_changes"`);
    await queryRunner.query(`DROP TABLE "movie_audit"`);
  }
}
