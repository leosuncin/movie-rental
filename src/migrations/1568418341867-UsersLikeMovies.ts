import { MigrationInterface, QueryRunner } from 'typeorm';

export class UsersLikeMovies1568418341867 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(`CREATE TABLE "movie_liked_by_user" (
          "movieId" uuid NOT NULL,
          "userId" uuid NOT NULL,
          CONSTRAINT "PK_55e9bc1b8a494faf0e04ff1e298"
            PRIMARY KEY ("movieId", "userId")
        )`);
    await queryRunner.query(
      `CREATE INDEX "IDX_4f6744f20c3a317cec9d82d1ec" ON "movie_liked_by_user" ("movieId")`,
    );
    await queryRunner.query(
      `CREATE INDEX "IDX_85222cc83195ba96ddb922b39b" ON "movie_liked_by_user" ("userId")`,
    );
    await queryRunner.query(`ALTER TABLE "movie_liked_by_user"
          ADD CONSTRAINT "FK_4f6744f20c3a317cec9d82d1ecb"
          FOREIGN KEY ("movieId")
          REFERENCES "movie"("id")
          ON DELETE CASCADE
          ON UPDATE NO ACTION`);
    await queryRunner.query(`ALTER TABLE "movie_liked_by_user"
          ADD CONSTRAINT "FK_85222cc83195ba96ddb922b39b7"
          FOREIGN KEY ("userId")
          REFERENCES "user"("id")
          ON DELETE CASCADE
          ON UPDATE NO ACTION`);
    await queryRunner.query(
      `ALTER TABLE "movie" ADD "likes" integer NOT NULL DEFAULT 0`,
    );
    await queryRunner.query(`CREATE FUNCTION update_likes_counter_for_movies()
          RETURNS trigger
          LANGUAGE plpgsql
          AS $$
            BEGIN
              UPDATE "movie"
              SET "likes" = (
                SELECT COUNT(DISTINCT "userId") FROM "movie_liked_by_user" WHERE "movieId" = NEW."movieId"
              )
              WHERE "id" = NEW."movieId";

              RETURN NEW;
            END;
          $$`);
    await queryRunner.query(`CREATE TRIGGER "update_movie_likes_counter"
          AFTER INSERT ON "movie_liked_by_user"
          FOR EACH ROW
          EXECUTE PROCEDURE update_likes_counter_for_movies()`);
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(
      `ALTER TABLE "movie_liked_by_user" DROP CONSTRAINT "FK_85222cc83195ba96ddb922b39b7"`,
    );
    await queryRunner.query(
      `ALTER TABLE "movie_liked_by_user" DROP CONSTRAINT "FK_4f6744f20c3a317cec9d82d1ecb"`,
    );
    await queryRunner.query(`DROP INDEX "IDX_85222cc83195ba96ddb922b39b"`);
    await queryRunner.query(`DROP INDEX "IDX_4f6744f20c3a317cec9d82d1ec"`);
    await queryRunner.query(
      `DROP TRIGGER "update_movie_likes_counter" ON "movie_liked_by_user"`,
    );
    await queryRunner.query(`DROP FUNCTION "update_likes_counter_for_movies"`);
    await queryRunner.query(`DROP TABLE "movie_liked_by_user"`);
    await queryRunner.query(`ALTER TABLE "movie" DROP COLUMN "likes"`);
  }
}
