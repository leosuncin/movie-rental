import { MigrationInterface, QueryRunner } from 'typeorm';

export class CreateMovie1568045462034 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(`CREATE TABLE "movie" (
          "id" uuid NOT NULL DEFAULT uuid_generate_v4(),
          "title" character varying NOT NULL,
          "description" character varying NOT NULL,
          "images" text NOT NULL,
          "stock" integer NOT NULL DEFAULT 0,
          "rentalPrice" numeric NOT NULL,
          "salePrice" numeric NOT NULL,
          "availability" boolean NOT NULL DEFAULT false,
          "slug" character varying NOT NULL,
          "year" integer NOT NULL,
          "rated" character varying NOT NULL,
          "releaseDate" TIMESTAMP NOT NULL,
          "runtime" character varying NOT NULL,
          "genre" text NOT NULL,
          "director" character varying NOT NULL,
          "writers" text NOT NULL,
          "actors" text NOT NULL,
          "production" character varying,
          "ratings" text NOT NULL,
          CONSTRAINT "UQ_454288774942b99d5127fb4173b" UNIQUE ("slug"),
          CONSTRAINT "PK_cb3bb4d61cf764dc035cbedd422" PRIMARY KEY ("id")
        )`);
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(`DROP TABLE "movie"`);
  }
}
