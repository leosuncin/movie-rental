import { MigrationInterface, QueryRunner } from 'typeorm';

export class CreatePurchaseAndDetails1568676463935
  implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(`CREATE TABLE "purchase" (
          "id" uuid NOT NULL DEFAULT uuid_generate_v4(),
          "total" numeric NOT NULL,
          "purchasedAt" TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
          "clientId" uuid NOT NULL,
          CONSTRAINT "PK_86cc2ebeb9e17fc9c0774b05f69" PRIMARY KEY ("id")
        )`);
    await queryRunner.query(`CREATE TABLE "purchase_detail" (
          "id" uuid NOT NULL DEFAULT uuid_generate_v4(),
          "quantity" integer NOT NULL DEFAULT 1,
          "price" numeric NOT NULL,
          "movieId" uuid NOT NULL,
          "purchaseId" uuid NOT NULL,
          CONSTRAINT "PK_b0988b2c3f2e1e5d95756b01389" PRIMARY KEY ("id")
        )`);
    await queryRunner.query(`ALTER TABLE "purchase"
          ADD CONSTRAINT "FK_73c8437825e1d3d8bae07550584"
          FOREIGN KEY ("clientId") REFERENCES "user" ("id")
          ON DELETE NO ACTION
          ON UPDATE NO ACTION`);
    await queryRunner.query(`ALTER TABLE "purchase_detail"
          ADD CONSTRAINT "FK_7bf31b824922b39b3fa4833c693"
          FOREIGN KEY ("movieId") REFERENCES "movie" ("id")
          ON DELETE NO ACTION
          ON UPDATE NO ACTION`);
    await queryRunner.query(`ALTER TABLE "purchase_detail"
          ADD CONSTRAINT "FK_6596b6e7b3814428ff49146cf5d"
          FOREIGN KEY ("purchaseId") REFERENCES "purchase" ("id")
          ON DELETE NO ACTION
          ON UPDATE NO ACTION`);
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(
      `ALTER TABLE "purchase_detail" DROP CONSTRAINT "FK_6596b6e7b3814428ff49146cf5d"`,
    );
    await queryRunner.query(
      `ALTER TABLE "purchase_detail" DROP CONSTRAINT "FK_7bf31b824922b39b3fa4833c693"`,
    );
    await queryRunner.query(
      `ALTER TABLE "purchase" DROP CONSTRAINT "FK_73c8437825e1d3d8bae07550584"`,
    );
    await queryRunner.query(`DROP TABLE "purchase_detail"`);
    await queryRunner.query(`DROP TABLE "purchase"`);
  }
}
