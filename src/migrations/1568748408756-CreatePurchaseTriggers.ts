import { MigrationInterface, QueryRunner } from 'typeorm';

export class CreatePurchaseTriggers1568748408756 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(`CREATE TRIGGER "check_and_decrement_stock_on_sell"
      BEFORE INSERT ON "purchase_detail"
      FOR EACH ROW
      EXECUTE PROCEDURE decrement_movie_stock()`);
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(
      `DROP TRIGGER "check_and_decrement_stock_on_sell" ON "purchase_detail"`,
    );
  }
}
