import { MigrationInterface, QueryRunner } from 'typeorm';

export class CreateRentAndDetails1568574003964 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(`CREATE TABLE "rent" (
        "id" uuid NOT NULL DEFAULT uuid_generate_v4(),
        "total" numeric NOT NULL,
        "returnDate" TIMESTAMP NOT NULL,
        "latePenalty" numeric NOT NULL DEFAULT 0.1,
        "returnedAt" TIMESTAMP,
        "totalPaid" numeric,
        "clientId" uuid NOT NULL,
        CONSTRAINT "PK_211f726fd8264e82ff7a2b86ce2" PRIMARY KEY ("id")
      )`);
    await queryRunner.query(`ALTER TABLE "rent"
        ADD CONSTRAINT "FK_e81219e1b1850301abf2438900c"
        FOREIGN KEY ("clientId") REFERENCES "user"("id")
        ON DELETE NO ACTION
        ON UPDATE NO ACTION`);
    await queryRunner.query(`CREATE TABLE "rent_detail" (
        "id" uuid NOT NULL DEFAULT uuid_generate_v4(),
        "quantity" integer NOT NULL DEFAULT 1,
        "price" numeric NOT NULL,
        "movieId" uuid NOT NULL,
        "rentId" uuid NOT NULL,
        CONSTRAINT "PK_4e1016403fdfe257dc4a7ae83f2" PRIMARY KEY ("id")
      )`);
    await queryRunner.query(`ALTER TABLE "rent_detail"
        ADD CONSTRAINT "FK_169db08fa5132fa94005d5c2503"
        FOREIGN KEY ("movieId") REFERENCES "movie"("id")
        ON DELETE NO ACTION
        ON UPDATE NO ACTION`);
    await queryRunner.query(`ALTER TABLE "rent_detail"
        ADD CONSTRAINT "FK_28adeabcfdeaefa820535abf437"
        FOREIGN KEY ("rentId") REFERENCES "rent"("id")
        ON DELETE NO ACTION
        ON UPDATE NO ACTION`);
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query(
      `ALTER TABLE "rent_detail" DROP CONSTRAINT "FK_28adeabcfdeaefa820535abf437"`,
    );
    await queryRunner.query(
      `ALTER TABLE "rent_detail" DROP CONSTRAINT "FK_169db08fa5132fa94005d5c2503"`,
    );
    await queryRunner.query(`DROP TABLE "rent_detail"`);
    await queryRunner.query(
      `ALTER TABLE "rent" DROP CONSTRAINT "FK_e81219e1b1850301abf2438900c"`,
    );
    await queryRunner.query(`DROP TABLE "rent"`);
  }
}
