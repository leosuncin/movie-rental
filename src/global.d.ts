declare module 'diffler' {
  /**
   * Read in two objects. Iterate over them and return the differences.
   *
   * @param {object} before First object to compare from.
   * @param {object} after Second object to compare against `before`.
   * @returns {object} Nested json object of changed properties containing a `from` and `to` key.
   */
  export default function(before: object, after: object): object;
}

declare module 'node-snowball' {
  export type SupportedLanguage =
    | 'arabic'
    | 'danish'
    | 'dutch'
    | 'english'
    | 'finnish'
    | 'french'
    | 'german'
    | 'hungarian'
    | 'italian'
    | 'norwegian'
    | 'portuguese'
    | 'spanish'
    | 'swedish'
    | 'romanian'
    | 'tamil'
    | 'turkish'
    | 'porter';
  export type SupportedEncoding = 'UTF-8' | 'ISO-8859-1' | 'ISO-8859-2';
  export function stemword<T = string | string[]>(
    word: T,
    language: SupportedLanguage = 'english',
    encoding: SupportedEncoding = 'UTF-8',
  ): T;
}

declare module 'lorca-nlp';

declare module 'stopword' {
  /**
   * Afrikaans
   */
  export const af: string[];
  /**
   * Modern Standard Arabic
   */
  export const ar: string[];
  /**
   * Bengali
   */
  export const bn: string[];
  /**
   * Brazilian Portuguese
   */
  export const br: string[];
  /**
   * Danish
   */
  export const da: string[];
  /**
   * German
   */
  export const de: string[];
  /**
   * English
   */
  export const en: string[];
  /**
   * Spanish
   */
  export const es: string[];
  /**
   * Farsi
   */
  export const fa: string[];
  /**
   * Finnish
   */
  export const fi: string[];
  /**
   * French
   */
  export const fr: string[];
  /**
   * Hausa
   */
  export const ha: string[];
  /**
   * Hebrew
   */
  export const he: string[];
  /**
   * Hindi
   */
  export const hi: string[];
  /**
   * Indonesian
   */
  export const id: string[];
  /**
   * Italian
   */
  export const it: string[];
  /**
   * Japanese
   */
  export const ja: string[];
  /**
   * Lugbara (without diacritics)
   */
  export const lgg: string[];
  /**
   * Lugbara official (with diacritics)
   */
  export const lggo: string[];
  /**
   * Dutch
   */
  export const nl: string[];
  /**
   * Norwegian
   */
  export const no: string[];
  /**
   * Polish
   */
  export const pl: string[];
  /**
   * Portuguese
   */
  export const pt: string[];
  /**
   * Punjabi Gurmukhi
   */
  export const pa: string[];
  /**
   * Russian
   */
  export const ru: string[];
  /**
   * Somali
   */
  export const so: string[];
  /**
   * Sotho
   */
  export const st: string[];
  /**
   * Swedish
   */
  export const sv: string[];
  /**
   * Swahili
   */
  export const sw: string[];
  /**
   * Vietnamese
   */
  export const vi: string[];
  /**
   * Yoruba
   */
  export const yo: string[];
  /**
   * Chinese Simplified
   */
  export const zh: string[];
  /**
   * Zulu
   */
  export const zu: string[];
  export function removeStopwords(text: string, stopwords?: string[]): string[];
}
